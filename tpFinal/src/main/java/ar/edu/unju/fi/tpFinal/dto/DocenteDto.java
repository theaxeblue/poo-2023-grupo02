package ar.edu.unju.fi.tpFinal.dto;

public class DocenteDto extends MiembroDto{
	private static final long serialVersionUID = 1L;
	private Integer legajo;

	public Integer getLegajo() {
		return legajo;
	}

	public void setLegajo(Integer legajo) {
		this.legajo = legajo;
	}

}
