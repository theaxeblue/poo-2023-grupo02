package ar.edu.unju.fi.tpFinal;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import ar.edu.unju.fi.tpFinal.dto.LibroDto;
import ar.edu.unju.fi.tpFinal.dto.MiembroDto;
import ar.edu.unju.fi.tpFinal.dto.PrestamoDto;
import ar.edu.unju.fi.tpFinal.exceptions.ManagerException;
import ar.edu.unju.fi.tpFinal.service.LibroService;
import ar.edu.unju.fi.tpFinal.service.MiembroService;
import ar.edu.unju.fi.tpFinal.service.PrestamoService;
import jakarta.mail.MessagingException;

@SpringBootTest
public class PrestamoServiceTest {

	@Autowired
	LibroService libroService;
	
	@Autowired
    MiembroService miembroService;
	
	@Autowired
	PrestamoService prestamoService;
	
	@Test
	@DisplayName("test de registro de prestamo")
	void testRegistrarPrestamo() throws MessagingException{
		Integer cantidad = prestamoService.obtenerListaPrestamos().size();
		LibroDto libroNuevo = new LibroDto();
		libroNuevo.setTitulo("prueba1");
		libroNuevo.setIsbn(10);
		libroNuevo.setNumeroInventario(10);
		libroNuevo.setEstado("Disponible");
		libroNuevo.setAutor("sandro");
		libroService.guardarLibro(libroNuevo);
		assertEquals(cantidad,prestamoService.obtenerListaPrestamos().size());
		//existe un miembro con el id 18 en la base de datos
	    //existia 9 prestamos entonces el nuevo a agregar tiene un id 10
		prestamoService.registrarPrestamo(18L,libroService.buscarPorIsbn(libroNuevo.getIsbn()).getId());
		assertNotNull(prestamoService.buscarPrestamoPorId(10L));
		cantidad = prestamoService.obtenerListaPrestamos().size();
	    Integer cantidadActual = prestamoService.obtenerListaPrestamos().size();
	    assertEquals(cantidadActual,cantidad);
	    assertTrue(prestamoService.convertirPrestamo(prestamoService.buscarPrestamoPorId(10L))
	    		.getLibro().getEstado().equals("Prestado"));
	    assertTrue(prestamoService.buscarPrestamoPorId(10L).getEstado().equals("En Prestamo"));
	}
	
	@Test
	@DisplayName("test de Modificar prestamo")
	void testModificacionPrestamo() throws MessagingException{
		// prestamos actuales en la base de datos
		Integer cantidad = prestamoService.obtenerListaPrestamos().size();
		//existe un prestamo con el Id 9
		PrestamoDto prestamo1 = prestamoService.buscarPrestamoPorId(9L);
		prestamo1.setEstado("Devuelto");
		prestamoService.modificarPrestamo(prestamo1);
		assertTrue(prestamoService.obtenerListaPrestamos().size() == cantidad);
		assertNotNull(prestamoService.buscarPrestamoPorId(9L));
		assertEquals(prestamo1.getEstado(),prestamoService.buscarPrestamoPorId(9L).getEstado());
	}
	@Test
	@DisplayName("test de eliminar prestamo")
	void testEliminarPrestamo(){
		//existe un prestamo con el Id 8 en la base de datos
		Integer cantidad = prestamoService.obtenerListaPrestamos().size();
		prestamoService.eliminarPorId(8L);
		assertTrue(cantidad > prestamoService.obtenerListaPrestamos().size());
		assertThrows(ManagerException.class,()->prestamoService.buscarPrestamoPorId(8L));
	}
	
	@Test
	@DisplayName("test de buscar prestamo")
	void testBuscarPrestamo() {
		//existe un prestamo con id 1
		assertNotNull(prestamoService.buscarPrestamoPorId(1L));
		//existe un prestamo con id 2
		assertNotEquals(prestamoService.buscarPrestamoPorId(1L).getTitulolibro(),
				prestamoService.buscarPrestamoPorId(2L).getTitulolibro());
		//no existe un prestamo con el id 55
		assertThrows(ManagerException.class,()->prestamoService.buscarPrestamoPorId(55L));
	}
	
	@Test
	@DisplayName("test de validaciones de prestamo")
	void testValidacionesPrestamo() {
	  	//Existe un miembro con el id 5
		MiembroDto miembro = miembroService.buscarPorId(5L);
		miembro.setDiasSancion(2);
		miembroService.editarMiembro(miembro);
		//existe un libro disponible con el id 3
		assertThrows(ManagerException.class,()->prestamoService.registrarPrestamo(5L,3L));
		//existe un miembro no sancionado con id 1
		//hay un libro con isbn 112 que ya esta prestado
		assertThrows(ManagerException.class,()->prestamoService.registrarPrestamo(1L,13L));
	}
}