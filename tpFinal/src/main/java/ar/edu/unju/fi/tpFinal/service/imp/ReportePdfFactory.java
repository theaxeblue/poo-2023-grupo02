package ar.edu.unju.fi.tpFinal.service.imp;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import ar.edu.unju.fi.tpFinal.service.AbstractFactory;
import ar.edu.unju.fi.tpFinal.service.ReporteService;

@Service("pdfFactory")
public class ReportePdfFactory implements AbstractFactory {

	@Autowired
	@Qualifier("pdf")
	ReporteService reportePdfService;
	
	/**
	 * metodo del tipo de fabrica para frabricar el objeto asignado
	 *@return el objeto que frabrica este tipo de frabrica
	 */
	public ReporteService crearReporte() {
		// TODO Auto-generated method stub
		return reportePdfService;
	}
}
