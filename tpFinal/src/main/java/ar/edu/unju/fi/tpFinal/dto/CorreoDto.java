package ar.edu.unju.fi.tpFinal.dto;

import java.io.Serializable;

public class CorreoDto implements Serializable {
   private static final long serialVersionUID = 1L;
   private String cuerpo;
   private String destinatario;
   private String asunto;

public String getCuerpo() {
	return cuerpo;
}
public void setCuerpo(String cuerpo) {
	this.cuerpo = cuerpo;
}
public String getDestinatario() {
	return destinatario;
}
public void setDestinatario(String destinatario) {
	this.destinatario = destinatario;
}
public String getAsunto() {
	return asunto;
}
public void setAsunto(String asunto) {
	this.asunto = asunto;
}
}
