package ar.edu.unju.fi.tpFinal.service;

import java.util.ArrayList;

import ar.edu.unju.fi.tpFinal.dto.LibroDto;
import ar.edu.unju.fi.tpFinal.entity.Libro;

public interface LibroService {
	/**
     * Guarda un libro en la biblioteca.
     *
     * @param libro El libro a ser guardado.
     */
	public LibroDto guardarLibro(LibroDto libroDto);
	/**
     * Modifica un libro existente en la biblioteca.
     *
     * @param libro El libro a ser modificado.
     */
	public LibroDto modificarLibro(LibroDto libroDto);
	 /**
     * Elimina un libro de la biblioteca.
     *
     * @param libro El libro a ser eliminado.
     */
	public LibroDto eliminarLibroPorIsbn(Integer isbn);
	/**
     * Obtiene una lista de todos los libros en la biblioteca.
     *
     * @return Una lista de libros.
     */
	public ArrayList<LibroDto> obtenerListaLibros();
	/**
     * Busca un libro por su id en la biblioteca.
     *
     * @param el id del libro a buscar.
     * @return El libroDto encontrado.
     */
	public LibroDto buscarPorId(Long id);
	/**
     * Busca un libro por su ISBN en la biblioteca.
     *
     * @param isbn El ISBN del libro a buscar.
     * @return El libro encontrado o null si no se encuentra.
     */
	public LibroDto buscarPorIsbn(Integer isbn);
	/**
     * Busca un libro por su autor en la biblioteca.
     *
     * @param autor El autor del libro a buscar.
     * @return El libro encontrado o null si no se encuentra.
     */
	public ArrayList<LibroDto> buscarPorAutor(String autor);
	/**
	 * convierte un libro a LibroDto
	 * @param el libro a convertir
	 * @return un objeto con los atributos de libro
	 */
	
	public LibroDto convertirLibroDto(Libro libro);
	/**
	 * convierte un libroDto a libro
	 * @param libroDto a convertir
	 * @return un objeto con los atributos de libroDto
	 */
	public Libro convertirLibro(LibroDto libroDto);
}