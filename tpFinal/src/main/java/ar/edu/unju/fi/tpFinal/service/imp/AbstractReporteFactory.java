package ar.edu.unju.fi.tpFinal.service.imp;

import org.springframework.stereotype.Service;

import ar.edu.unju.fi.tpFinal.service.AbstractFactory;
import ar.edu.unju.fi.tpFinal.service.ReporteService;

@Service
public class AbstractReporteFactory {

	/**
	 * metodo que sirve de utilidad para constuir al objeto en base
	 *  a la fabrica
	 * @param tipo de frabica para generar resumen excel o pdf
	 * @return el objeto con los metodos para generar el resumen
	 */
	public ReporteService obtenerReporte(AbstractFactory fabrica){
		return fabrica.crearReporte();
	}
}
