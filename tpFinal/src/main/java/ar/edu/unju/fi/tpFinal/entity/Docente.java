package ar.edu.unju.fi.tpFinal.entity;

import jakarta.persistence.Column;
import jakarta.persistence.DiscriminatorValue;
import jakarta.persistence.Entity;

@Entity
@DiscriminatorValue(value = "docente")
public class Docente extends Miembro{
    
	@Column(name="Legajo")
	private Integer legajo;

	public Integer getLegajo() {
		return legajo;
	}

	public void setLegajo(Integer legajo) {
		this.legajo = legajo;
	}

	@Override
	public String toString() {
		return super.toString()+"Docente [legajo=" + legajo + "]";
	}
}
