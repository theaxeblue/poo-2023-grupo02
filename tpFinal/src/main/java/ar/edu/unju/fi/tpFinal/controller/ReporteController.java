package ar.edu.unju.fi.tpFinal.controller;


import java.io.FileNotFoundException;
import java.io.IOException;
import java.time.ZoneId;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.databind.ObjectMapper;

import ar.edu.unju.fi.tpFinal.exceptions.ManagerException;
import ar.edu.unju.fi.tpFinal.service.AbstractFactory;
import ar.edu.unju.fi.tpFinal.service.ComprobanteService;
import ar.edu.unju.fi.tpFinal.service.ReporteService;
import ar.edu.unju.fi.tpFinal.service.imp.AbstractReporteFactory;
import jakarta.servlet.http.HttpServletResponse;

@RestController
@RequestMapping("/biblioteca/v1/reporte")
public class ReporteController {
	
	private final Logger logger = LoggerFactory.getLogger(ReporteController.class);
	@Autowired
	AbstractReporteFactory fabrica;
	@Autowired
	@Qualifier("excelFactory")
	AbstractFactory excelFactory;
	@Autowired
	@Qualifier("pdfFactory")
	AbstractFactory pdfFactory;
	@Autowired
	ComprobanteService comprobanteService;
	@Autowired
    private ObjectMapper objectMapper;

	/**
     * Genera un informe resumen en formato Excel para un rango de fechas dado.
     * 
     * @param resumenPrestamo La respuesta HTTP donde se escribirá el archivo Excel.
     * @param fechaInicio La fecha de inicio del rango.
     * @param fechaFin La fecha de fin del rango.
     */
    @GetMapping("/resumen/excel/{fechaInicio}/{fechaFin}")
    	public void generarExcelResumen(HttpServletResponse resumenPrestamo, @PathVariable @DateTimeFormat(pattern = "yyyy-MM-dd") Date fechaInicio,
            @PathVariable @DateTimeFormat(pattern = "yyyy-MM-dd") Date fechaFin) throws IOException{
    	Map<String,Object> response = new HashMap<String,Object>();
    	try {
        resumenPrestamo.setContentType("application/octet-stream");
        String headerkey = "Content-Disposition";
        String headerValue = "attachment;filename=resumen.xlsx";
        resumenPrestamo.setHeader(headerkey, headerValue);
        ReporteService excelFabricado = fabrica.obtenerReporte(excelFactory);
        excelFabricado.generarResumen(resumenPrestamo, fechaInicio, fechaFin);
        logger.info("se envio un archivo de resumen de prestamo al cliente con las fechas: "+fechaInicio+" "
        		+ "hasta "+ fechaFin + " en formato excel");
    	}catch(ManagerException e){
    		response.put("Mensaje","ocurrio un error  al intentar generar el resumen de prestamo"
    				+ "desde las fechas: "+fechaInicio.toInstant().atZone(ZoneId.systemDefault()).toLocalDate()+" hasta "+fechaFin.toInstant().atZone(ZoneId.systemDefault()).toLocalDate() + " en formato excel");
    		response.put("Error",e.getMessage());
    		resumenPrestamo.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            resumenPrestamo.setHeader("Content-Type", "application/json");
            resumenPrestamo.getWriter().write(objectMapper.writeValueAsString(response));
    	}
    }
    /**
     * Genera un informe resumen en formato PDF para un rango de fechas dado.
     * 
     * @param resumenPrestamo La respuesta HTTP donde se escribirá el archivo PDF.
     * @param fechaInicio La fecha de inicio del rango.
     * @param fechaFin La fecha de fin del rango.
     */
    @GetMapping("/resumen/pdf/{fechaInicio}/{fechaFin}")
	public void generarPdfResumen(HttpServletResponse resumenPrestamo, @PathVariable @DateTimeFormat(pattern = "yyyy-MM-dd") Date fechaInicio,
        @PathVariable @DateTimeFormat(pattern = "yyyy-MM-dd") Date fechaFin)throws IOException{
    	Map<String,Object> response = new HashMap<String,Object>();
    try {	
    resumenPrestamo.setContentType("application/pdf");
    String headerkey = "Content-Disposition";
    String headerValue = "attachment;filename=resumen.pdf";
    resumenPrestamo.setHeader(headerkey, headerValue);
    ReporteService pdfFabricado = fabrica.obtenerReporte(pdfFactory);
    pdfFabricado.generarResumen(resumenPrestamo,fechaInicio,fechaFin);
    logger.info("se envio un archivo de resumen de prestamo al cliente con las fechas: "+fechaInicio+" "
    		+ "hasta "+ fechaFin + " en formato pdf");
    }catch(ManagerException e){
    	response.put("Mensaje","ocurrio un error  al intentar generar el resumen de prestamo"
				+ "desde las fechas: "+fechaInicio.toInstant().atZone(ZoneId.systemDefault()).toLocalDate()+" hasta "+fechaFin.toInstant().atZone(ZoneId.systemDefault()).toLocalDate() + " en formato pdf");
		response.put("Error",e.getMessage());
		resumenPrestamo.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
        resumenPrestamo.setHeader("Content-Type", "application/json");
        resumenPrestamo.getWriter().write(objectMapper.writeValueAsString(response));
    }
   }
    /**
     * Genera un comprobante de préstamo en formato PDF para un préstamo específico.
     * 
     * @param comprobantePrestamo La respuesta HTTP donde se escribirá el archivo PDF.
     * @param id El identificador del préstamo.
     * @throws IOException 
     * @throws FileNotFoundException 
     */
    @GetMapping("/comprobante/{id}")
    public void generarComprobante(HttpServletResponse comprobantePrestamo,
    		@PathVariable Long id ) throws ManagerException, IOException{
    	Map<String,Object> response = new HashMap<String,Object>();
    	try {
    	    comprobantePrestamo.setContentType("application/pdf");
    	    String headerkey = "Content-Disposition";
    	    String headerValue = "attachment;filename=comprobante.pdf";  	    
    	    comprobantePrestamo.setHeader(headerkey, headerValue);
    	    comprobanteService.generarComprobante(comprobantePrestamo, id);
    	    comprobantePrestamo.setStatus(HttpServletResponse.SC_OK);
    	    logger.info("se creo genero un archivo pdf para el cliente con el prestamo id: "+id);
    	}catch(ManagerException e){
    		response.put("Mensaje","ocurrio un error  al intentar generar el comprobante de prestamo"
    				+ "con el id: "+id );
    		response.put("Error",e.getMessage());
    		comprobantePrestamo.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            comprobantePrestamo.setHeader("Content-Type", "application/json");
            comprobantePrestamo.getWriter().write(objectMapper.writeValueAsString(response));
    	}
    }
}