package ar.edu.unju.fi.tpFinal.service.imp;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ar.edu.unju.fi.tpFinal.dto.DevolucionDto;
import ar.edu.unju.fi.tpFinal.entity.Devolucion;
import ar.edu.unju.fi.tpFinal.entity.Libro;
import ar.edu.unju.fi.tpFinal.entity.Miembro;
import ar.edu.unju.fi.tpFinal.entity.Prestamo;
import ar.edu.unju.fi.tpFinal.exceptions.ManagerException;
import ar.edu.unju.fi.tpFinal.repository.DevolucionRepository;
import ar.edu.unju.fi.tpFinal.repository.LibroRepository;
import ar.edu.unju.fi.tpFinal.repository.MiembroRepository;
import ar.edu.unju.fi.tpFinal.repository.PrestamoRepository;
import ar.edu.unju.fi.tpFinal.service.DevolucionService;
import ar.edu.unju.fi.tpFinal.service.PrestamoService;

@Service
public class DevolucionServiceImp implements DevolucionService {

	public static Logger logger = Logger.getLogger(PrestamoServiceImp.class);
	@Autowired
	PrestamoRepository prestamoRepository;
	@Autowired
	DevolucionRepository devolucionRepository;
	@Autowired
	LibroRepository libroRepository;
	@Autowired
	MiembroRepository miembroRepository;
	@Autowired
	PrestamoService prestamoService;
	
	/**
     * Registra una devolución a partir de la información proporcionada en un
     * objeto DevolucionDto.
     *
     * @param devolucionDto Objeto DevolucionDto con la información de la devolución.
     * @return DevolucionDto con la información de la devolución registrada.
     * @throws ManagerException Si se intenta registrar una devolución con un
     *                          préstamo nulo o inválido.
     */
	@Override
	public DevolucionDto registrarDevolucion(Long id)throws ManagerException{
		// TODO Auto-generated method stub
		if(prestamoService.buscarPrestamoPorId(id) == null) {
			logger.error("se intento registrar una devolucion con un prestamo de id nulo");
			throw new ManagerException("se ingreso un prestamo invalido");
		}
		Devolucion devolucion = prepararDevolucion(id);
	    devolucionRepository.save(devolucion);
	    logger.info("se registro una devolucion con el id de: "+devolucion.getId());
	    marcarPrestamoComoDevuelto(devolucion);
		return convertirDevolucionDto(devolucion);
	}
	 /**
     * Obtiene una lista de objetos DevolucionDto que representan todas las
     * devoluciones registradas.
     *
     * @return Lista de objetos DevolucionDto.
     */
	public List<DevolucionDto> obtenerListaDevolucion(){
		List<DevolucionDto> listaDto = new ArrayList<DevolucionDto>();
		List<Devolucion> listaDevolucion = devolucionRepository.findAll();
		logger.info("se obtiene la lista de devoluciones con el tamaño de: "+listaDto.size());
		for(Devolucion dev : listaDevolucion){
		    listaDto.add(convertirDevolucionDto(dev));	
		}
		return listaDto;
	}
	 /**
     * Modifica la información de una devolución a partir de un objeto
     * DevolucionDto.
     *
     * @param devolucionDto Objeto DevolucionDto con la información actualizada.
     * @return DevolucionDto con la información de la devolución modificada.
     * @throws ManagerException Si se intenta modificar una devolución con un id nulo
     *                          o con un préstamo nulo o inválido.
     */
	public DevolucionDto modificarDevolucion(DevolucionDto devolucionDto)throws ManagerException{
		if(devolucionDto.getId() == null) {
			logger.error("se intento modificar una devolucion con un id nulo");
			throw new ManagerException("NO ingreso un id de Devolucion");
		}
		if(prestamoService.buscarPrestamoPorId(devolucionDto.getPrestamo()) == null) {
			logger.error("se intento modificar una devolucion con un id invalido: "+devolucionDto.getId());
			throw new ManagerException("Se ingreso un id de prestamo invalido");
		}
		Devolucion devolucion = convertirDevolucion(devolucionDto);
		devolucionRepository.save(devolucion);
		logger.info("se modificar la devolucion con el id: "+ devolucion.getId());
		return convertirDevolucionDto(devolucionRepository.findById(devolucionDto.getId()).get());
	}
	 /**
     * Busca una devolución por su ID.
     *
     * @param id ID de la devolución a buscar.
     * @return DevolucionDto con la información de la devolución encontrada.
     * @throws ManagerException Si no se encuentra una devolución con el ID
     *                          proporcionado.
     */
	@Override
	public DevolucionDto buscarDevolucionPorId(Long id) throws ManagerException{
		if(devolucionRepository.findById(id).orElse(null) == null) {
			logger.error("se intento buscar una devolucion con un id invalido: "+id);
			throw new ManagerException("no se encontro la devolucion con ese id");
		}
		logger.info("se encontro la devolucion a buscar con el id: "+id);
		return convertirDevolucionDto(devolucionRepository.findById(id).get());
	}
	/**
     * Prepara una devolución a partir de un objeto DevolucionDto.
     *
     * @param devolucionDto Objeto DevolucionDto con la información de la devolución.
     * @return Devolucion preparada.
     */
	public Devolucion prepararDevolucion(Long id){
		logger.debug("se prepara una devolucion");
		DevolucionDto devolucionDto = new DevolucionDto();
		devolucionDto.setPrestamo(id);;
		Devolucion devolucion = new Devolucion();
		Date fechaActual = new Date(System.currentTimeMillis());
	    devolucion = convertirDevolucion(devolucionDto);
	    devolucion.setDevolucionEfectuada(fechaActual);
	    marcarLibroComoDisponible(devolucion.getPrestamo().getLibro());
	    sancionarMiembro(devolucion);
	    logger.debug("asignando nuevo id");
	    Integer id2 = obtenerListaDevolucion().size()+1;
	    Long id3 = Long.valueOf(id2);
	    devolucion.setId(id3);
	    logger.info("se preparo la devolucion con el id: "+devolucion.getId());
		return devolucion;
	}
	/**
     * Aplica la sanción correspondiente a un miembro según la diferencia entre la
     * fecha de devolución y la fecha límite de devolución del préstamo.
     *
     * @param devolucion Devolución a partir de la cual se calcula la sanción.
     */
	public void sancionarMiembro(Devolucion devolucion){
		logger.debug("se le aplica al miembro su respectivo nivel de sancion");
		Miembro miembroSancion = miembroRepository.findById(devolucion.getPrestamo().getMiembro().getId()).get();
		Long diferencia = devolucion.getPrestamo().getFechaYhoraDevolucion().getTime() -devolucion.getDevolucionEfectuada().getTime();
		Long diferenciaEnDias = ((diferencia / (24 * 60 * 60 * 1000)))*-1;
        logger.debug("la diferencia fue: "+ diferenciaEnDias);
        logger.debug(devolucion.getPrestamo().getFechaYhoraDevolucion().getTime());
        logger.debug(devolucion.getDevolucionEfectuada().getTime());
        if(devolucion.getDevolucionEfectuada().getTime() >= devolucion.getPrestamo().getFechaYhoraDevolucion().getTime()){
        if(diferenciaEnDias <= 2){
        	miembroSancion.setDiasSancion(3);
         }else{
        	if(diferenciaEnDias >= 3 && diferenciaEnDias <=5){
        		miembroSancion.setDiasSancion(5);	
        	}else{
        		if(diferenciaEnDias >= 6){
        			miembroSancion.setDiasSancion(20);
        		}else {
        			miembroSancion.setDiasSancion(0);
        		}
        	}
         }
        }else {
        	miembroSancion.setDiasSancion(0);
        }
        miembroRepository.save(miembroSancion);
        logger.info("la sancion fue de: "+ devolucion.getPrestamo().getMiembro().getDiasSancion()+
        		" al miembro de id: "+ devolucion.getPrestamo().getMiembro().getId());
	  }
	
	/**
     * Marca un libro como disponible cambiando su estado a "Disponible".
     *
     * @param libro Libro a marcar como disponible.
     */
	public void marcarLibroComoDisponible(Libro libro) {
		logger.debug("se marca el libro como disponible");
		Libro libroMarcado = libroRepository.findById(libro.getId()).get();
		libroMarcado.setEstado("Disponible");
		libroRepository.save(libroMarcado);
		logger.info("se marco al libro como disponible con el id de: "+libroMarcado.getId());
	}
	/**
     * Marca un préstamo como devuelto cambiando su estado a "Devuelto".
     *
     * @param devolucion Devolución asociada al préstamo a marcar como devuelto.
     */
	public void marcarPrestamoComoDevuelto(Devolucion devolucion){
		logger.debug("se marca el prestamo como Devuelto");
		Prestamo prestamo = devolucion.getPrestamo();
		prestamo.setEstado("Devuelto");
		prestamo.setDevolucion(devolucionRepository.findById(devolucion.getId()).get());
		prestamoRepository.save(prestamo);
		logger.info("se marca el prestamo como devuelto con el id de: "+prestamo.getId());
	}
	/**
     * Convierte una devolución a un objeto DevolucionDto.
     *
     * @param devolucion Devolución a convertir.
     * @return DevolucionDto con la información de la devolución.
     */
	public DevolucionDto convertirDevolucionDto(Devolucion devolucion) {
		logger.info("se convierte una devolucion a una devolucionDto");
		DevolucionDto devolucionDto = new DevolucionDto();
		devolucionDto.setPrestamo(devolucion.getPrestamo().getId());
		devolucionDto.setDevolucionEfectuada(devolucion.getDevolucionEfectuada());
		devolucionDto.setNombreMiembro(devolucion.getPrestamo().getMiembro().getNombre());
		devolucionDto.setLibroTitulo(devolucion.getPrestamo().getLibro().getTitulo());
		devolucionDto.setId(devolucion.getId());
		return devolucionDto;
	}
	/**
     * Convierte un objeto DevolucionDto a una devolución.
     *
     * @param devolucionDto Objeto DevolucionDto a convertir.
     * @return Devolucion convertida.
     */
	public Devolucion convertirDevolucion(DevolucionDto devolucionDto){
		logger.info("se convierte una devolucionDto a una devolucion");
			Devolucion devolucion = new Devolucion();
			devolucion.setId(devolucionDto.getId());
			devolucion.setDevolucionEfectuada(devolucionDto.getDevolucionEfectuada());
		    devolucion.setPrestamo(prestamoRepository.findById(devolucionDto.getPrestamo()).orElse(null));
			return devolucion;
	}
}