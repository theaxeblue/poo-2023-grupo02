package ar.edu.unju.fi.tpFinal.service;

import java.util.ArrayList;

import ar.edu.unju.fi.tpFinal.dto.MiembroDto;
import ar.edu.unju.fi.tpFinal.entity.Miembro;

public interface MiembroService {

	 /**
     * Guarda un miembro en la biblioteca.
     *
     * @param miembro El miembro a ser guardado.
     */
	public MiembroDto guardarMiembro(MiembroDto miembroDto);
	/**
     * Edita un miembro existente en la biblioteca.
     *
     * @param miembro El miembro a ser editado.
     */
	public MiembroDto editarMiembro(MiembroDto miembroDto);
	/**
     * Elimina un miembro de la biblioteca.
     *
     * @param miembro El miembro a ser eliminado.
     */
	public MiembroDto eliminarMiembroPorId(Long id);
	/**
     * Obtiene una lista de todos los miembros en la biblioteca.
     *
     * @return Una lista de miembros.
     */
	public ArrayList<MiembroDto> obtenerListaMiembros();
	/**
     * Busca un miembro por su código en la biblioteca.
     *
     * @param codigo El código del miembro a buscar.
     * @return El miembro encontrado o null si no se encuentra.
     */
	public MiembroDto buscarPorId(Long Id);
	/**
     * Busca un miembro por su NumeroMiembro en la biblioteca.
     *
     * @param numeroMiembro a buscar.
     * @return El miembro encontrado o null si no se encuentra.
     */
	public MiembroDto buscarPorNumeroMiembro(Integer numeroMiembro);
	/**
     * elimina un miembro de la base de datos segun su numeroMiembro
     *
     * @param numeroMiembro coincidente para eliminar
     */
	public MiembroDto eliminarPorNumeroMiembro(Integer numeroMiembro);
	 /**
 	 * convierte miembro a MiembroDto sea de las clases hijas
 	 * Alumno o Docente
 	 * @param el miembro a convertir
 	 * @return un objeto con los atributos de miembro o null
 	 */
	public MiembroDto convertirMiembroDto(Miembro miembro);
	/**
 	 * convierte miembroDto a Miembro sea de las clases hijas
 	 * Alumno o Docente
 	 * @param el miembroDto a convertir
 	 * @return un objeto con los atributos de miembro O null
 	 */
	public Miembro convertirMiembro(MiembroDto miembroDto);
}