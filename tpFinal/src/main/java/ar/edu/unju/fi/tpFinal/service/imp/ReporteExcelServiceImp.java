package ar.edu.unju.fi.tpFinal.service.imp;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;
import java.util.ArrayList;
import java.util.List;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ar.edu.unju.fi.tpFinal.dto.PrestamoDto;
import ar.edu.unju.fi.tpFinal.exceptions.ManagerException;
import ar.edu.unju.fi.tpFinal.repository.DevolucionRepository;
import ar.edu.unju.fi.tpFinal.service.DevolucionService;
import ar.edu.unju.fi.tpFinal.service.PrestamoService;
import ar.edu.unju.fi.tpFinal.service.ReporteService;
import jakarta.servlet.ServletOutputStream;
import jakarta.servlet.http.HttpServletResponse;

@Service("excel")
public class ReporteExcelServiceImp implements ReporteService{

	public static Logger logger = Logger.getLogger(ReporteExcelServiceImp.class);
	@Autowired
	PrestamoService prestamoService;
	@Autowired
	DevolucionService devolucionService;
	@Autowired
	DevolucionRepository devolucionRepository;
	
	/**
     * Genera un resumen de préstamos filtrados por fechas y lo guarda en el objeto HttpServletResponse.
     * Utiliza la biblioteca Apache POI para crear un archivo Excel.
     * 
     * @param resumenPrestamo Objeto HttpServletResponse donde se guardará el informe generado.
     * @param fechaInicio Fecha de inicio para filtrar los préstamos.
     * @param fechaFin Fecha de fin para filtrar los préstamos.
     */
	@Override	
	public void generarResumen(HttpServletResponse resumenPrestamo,Date fechaInicio, Date fechaFin) throws SecurityException{
		if(fechaInicio.getTime() > fechaFin.getTime()) {
			logger.error("la fecha inicio: "+fechaInicio+ " es mayor a la fecha final: "+fechaFin);
			throw new ManagerException("la fecha inicio no puede ser mayor a la fecha final");
		}
		List<PrestamoDto> listaFiltrada = obtenerPrestamosFiltrados(fechaInicio,fechaFin);
		logger.info("cantidad de lista filtrada: "+ listaFiltrada.size());
		List<String> campos = obtenerListaCampos();
		XSSFWorkbook libro = (XSSFWorkbook) crearTabla(listaFiltrada,campos);
		try {
		ServletOutputStream ops = resumenPrestamo.getOutputStream();
		libro.write(ops);
		libro.close();
		ops.close();
		logger.info("se escribio y cerro el libro correctamente para generar el resumen excel");
		}catch(IOException e) {
			logger.error("ocurrio un error para la respuesta http: "+e.getMessage());
	    	throw new ManagerException("Error al escribir en la respuesta HTTP"+ e.getMessage());
		}
	}
	
	/**
	 * metodo que devuelve los campos de prestamos necesarios
	 * para la tabla de prestamo
	 * @return la lista de campos
	 */
	public List<String> obtenerListaCampos(){
		List<String> campos = new ArrayList<>();
		campos.add("id");
		campos.add("fechaYhoraPrestamo");
		campos.add("fechaYhoraDevolucion");
		campos.add("nombreMiembro");
		campos.add("TituloLibro");
		campos.add("estado");
		campos.add("DevolucionEfectuada");
		logger.info("se genera una lista de campos con el tamaño: "+campos.size());
		return campos;
	}
	/** 
	 * este metodo genera una tabla con los respectivos valores
	 * de los campos y asignando valores de los registros
	 * @param listaFiltrada, los registros a asignar a la tabla
	 * @param los campos para armar la tabla
	 * @return el objecto libro de excel con la tabla ya asignada
	 */
	public Object crearTabla(List<PrestamoDto> listaFiltrada,List<String> campos){
		XSSFWorkbook libro = new XSSFWorkbook();
		XSSFSheet hoja = libro.createSheet("Resumen");
		XSSFRow fila = null;
		XSSFCell celda = null;
		//estilos
		    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM MMMM-dd EEEE");
			CellStyle headerStyle = libro.createCellStyle();
			Font headerFont = libro.createFont();
			headerFont.setBold(true);
			headerStyle.setFont(headerFont);
			headerStyle.setFillForegroundColor(IndexedColors.LIGHT_BLUE.getIndex());
		    headerStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
			CellStyle dateStyle = libro.createCellStyle();
			dateStyle.setAlignment(HorizontalAlignment.CENTER);
			dateStyle.setFillForegroundColor(IndexedColors.LIGHT_GREEN.getIndex());
		    dateStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
			CellStyle bigTextStyle = libro.createCellStyle();
			CellStyle titleStyle = libro.createCellStyle();
			titleStyle.setAlignment(HorizontalAlignment.CENTER);
		    Font fontTitle = libro.createFont();
		    Font bigTextFont = libro.createFont();
		    bigTextFont.setFontHeightInPoints((short) 15); // Tamaño de la letra
		    bigTextStyle.setFont(bigTextFont);
		    bigTextStyle.setFillForegroundColor(IndexedColors.LIGHT_GREEN.getIndex());
		    bigTextStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
		    fontTitle.setFontHeightInPoints((short)20);
		    titleStyle.setFont(fontTitle);
		    titleStyle.setFillForegroundColor(IndexedColors.ORANGE.getIndex());
		    titleStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
		    dateStyle.setFont(bigTextFont);
		 // Crear una fila para el encabezado
		    fila = hoja.createRow(0);
		    XSSFCell titleCell = fila.createCell(0);
		    titleCell.setCellValue("RESUMEN DE PRÉSTAMO");
		    titleCell.setCellStyle(titleStyle);
		    hoja.addMergedRegion(new CellRangeAddress(0, 0, 0,(campos.size()-1))); // Fusionar celdas para el título		    
		for(int i = 1;i<listaFiltrada.size()+1;i++){
			if(i == 1){
				fila = hoja.createRow(1);
				for(int j=0;j < campos.size();j++) {
					celda = fila.createCell(j);
					celda.setCellValue(campos.get(j));
					celda.setCellStyle(headerStyle);
					celda.setCellStyle(titleStyle);
				}
			}
			PrestamoDto prestamo  = listaFiltrada.get(i-1);
			List<Object> atributos = obtenerAtributos(prestamo);
			fila = hoja.createRow(i+1);
			for(int a=0;a < atributos.size();a++) {
				celda = fila.createCell(a);
				if(atributos.get(a) instanceof Long) {
					celda.setCellValue((Long) atributos.get(a));
					celda.setCellStyle(bigTextStyle);
				}
                 if(atributos.get(a) instanceof String) {
                	 celda.setCellValue((String) atributos.get(a));
                	 celda.setCellStyle(bigTextStyle);
				}
               if(atributos.get(a) instanceof Date) {
                   String fechaFormateada = dateFormat.format((Date) atributos.get(a));
            	   celda.setCellValue(fechaFormateada);
            	   celda.setCellStyle(dateStyle);
                }
               hoja.autoSizeColumn(a);
			}
		}
	    // Crear una fila para la fecha
	    hoja.addMergedRegion(new CellRangeAddress(listaFiltrada.size() + 2, listaFiltrada.size() + 2, 0, campos.size() - 1)); // Fusionar celdas para la fecha
	    XSSFRow dateRow = hoja.createRow(listaFiltrada.size()+2);
	    XSSFCell dateCell = dateRow.createCell(0);
	    dateCell.setCellValue("Fecha del Resumen: " +dateFormat.format(Date.from(LocalDate.now().atStartOfDay(ZoneId.systemDefault()).toInstant())));
	    dateCell.setCellStyle(dateStyle);
	    logger.info("se obtiene el libro con la tabla de resumen excel");
		return libro;
	}

	 /**
     * Filtra la lista de préstamos obtenida del servicio para incluir solo aquellos
     * que se encuentran en el rango de fechas especificado.
     * 
     * @param fechaInicio Fecha de inicio para el filtro.
     * @param fechaFin Fecha de fin para el filtro.
     * @return Lista filtrada de préstamos.
     */
	public List<PrestamoDto> obtenerPrestamosFiltrados(Date fechaInicio, Date fechaFin){
		// TODO Auto-generated method stub
		List<PrestamoDto> listaPrestamo = prestamoService.obtenerListaPrestamos();
		List<PrestamoDto> listaFiltrada = new ArrayList<PrestamoDto>();
		for(PrestamoDto presta : listaPrestamo){
			if((presta.getFechaYhoraPrestamo().getTime() >= fechaInicio.getTime())&&
					(presta.getFechaYhoraPrestamo().getTime() <= fechaFin.getTime())){
				listaFiltrada.add(presta);	
			}
		}
		logger.info("se obtiene una lista de prestamos del tamaño: "+listaFiltrada.size());
		return listaFiltrada;
	}

	/**
     * Obtiene una lista de atributos relevantes de un objeto PrestamoDto.
     * 
     * @param prestamoDto Objeto PrestamoDto del cual se obtendrán los atributos.
     * @return Lista de atributos del objeto PrestamoDto.
     */
	@Override
	public List<Object> obtenerAtributos(PrestamoDto prestamoDto) {
		List<Object> lista = new ArrayList<>();
		lista.add(prestamoDto.getId());
		lista.add(prestamoDto.getFechaYhoraPrestamo());
		lista.add(prestamoDto.getFechaYhoraDevolucion());
		lista.add(prestamoDto.getNombreMiembro());
		lista.add(prestamoDto.getTitulolibro());
		lista.add(prestamoDto.getEstado());
		if(prestamoDto.getDevolucion() != null) {
			lista.add(devolucionRepository.findById(prestamoDto.getDevolucion()).get().getDevolucionEfectuada());
		}else {
		   lista.add("No se Devolvio todavia");
		}
		logger.info("obtiene una lista de los atributos del prestamo con tamaño: "+lista.size());
		return lista;
	}
}
