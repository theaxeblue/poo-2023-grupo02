package ar.edu.unju.fi.tpFinal.service.imp;

import java.sql.Date;
import java.util.ArrayList;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ar.edu.unju.fi.tpFinal.dto.CorreoDto;
import ar.edu.unju.fi.tpFinal.dto.PrestamoDto;
import ar.edu.unju.fi.tpFinal.entity.Libro;
import ar.edu.unju.fi.tpFinal.entity.Prestamo;
import ar.edu.unju.fi.tpFinal.exceptions.ManagerException;
import ar.edu.unju.fi.tpFinal.repository.DevolucionRepository;
import ar.edu.unju.fi.tpFinal.repository.LibroRepository;
import ar.edu.unju.fi.tpFinal.repository.MiembroRepository;
import ar.edu.unju.fi.tpFinal.repository.PrestamoRepository;
import ar.edu.unju.fi.tpFinal.service.CorreoService;
import ar.edu.unju.fi.tpFinal.service.LibroService;
import ar.edu.unju.fi.tpFinal.service.MiembroService;
import ar.edu.unju.fi.tpFinal.service.PrestamoService;
import ar.edu.unju.fi.tpFinal.util.Util;
import jakarta.mail.MessagingException;
import jakarta.transaction.Transactional;

@Service
public class PrestamoServiceImp implements PrestamoService {
	public static Logger logger = Logger.getLogger(PrestamoServiceImp.class);

	@Autowired
	LibroRepository libroRepository;
	@Autowired
	MiembroRepository miembroRepository;
	@Autowired
	PrestamoRepository prestamoRepository;
	@Autowired
	DevolucionRepository devolucionRepository;
	@Autowired
	CorreoService correoService;
	@Autowired
	LibroService libroService;
	@Autowired
	MiembroService miembroService;
	/**
	 * Se guarda el registro de un prestamo en la base de datos
	 * @param prestamo El prestamo a ser registrado en la base de datos
	 */
	@Override
	public PrestamoDto registrarPrestamo(Long idMiembro, Long idLibro) throws ManagerException, MessagingException{
		// TODO Auto-generated method stub
		if((libroService.buscarPorId(idLibro)).getEstado().equals("Prestado")){
			logger.error("EL libro ya está registrado como prestado. id: " +idLibro);
			throw new ManagerException("El libro ya está prestado.");
		}
		if(miembroRepository.findById(idMiembro).get().getDiasSancion()>0){
			logger.error("el miembro asociado al prestamo esta sancionado. id: "+idMiembro);
			throw new ManagerException("se intengo registrar un prestamo con un miembro sancionado");
		}
		Prestamo prestamo = prepararPrestamo(idMiembro,idLibro);
		prestamoRepository.save(prestamo);
		logger.info("Se registra un nuevo prestamo con el id: "+ prestamo.getId());
		marcarLibroComoPrestado(prestamo);
		mandarCorreoPrestamo(prestamo);
		return convertirPrestamoDto(prestamo);
	}
	/**
	 * Modifica el registro de un prestamo existente
	 * @param prestamo el prestamo a ser modificado
	 */
	@Transactional
	@Override
	public PrestamoDto modificarPrestamo(PrestamoDto prestamoDto) throws ManagerException{
		// TODO Auto-generated method stub
		if(prestamoDto.getId() == null){
			logger.error("se intento modificar un prestamo con un id nulo");
			throw new ManagerException("falto ingresar el id del prestamo a modificar");
		}
		if(prestamoRepository.findById(prestamoDto.getId()).orElse(null) == null){
			logger.error("se intento modificar un prestamo con un id invalido: "+ prestamoDto.getId());
			throw new ManagerException("el id de prestamo que ingreso es invalido"
					+ " no se encuentra en la base de datos");
		}
		Prestamo prestamo = convertirPrestamo(prestamoDto);
		logger.info("Se modifica un prestamo con el id: "+prestamo.getId());
		prestamoRepository.save(prestamo);
		prestamoDto = convertirPrestamoDto(prestamoRepository.findById(prestamo.getId()).get());
		return prestamoDto;
	}

	/**
	 * Elimina un registro de prestamo de la base de datos.
	 * @param nroInventarioLibro Es el numero de inventario del libro del prestamo a ser eliminado.
	 */
	@Transactional
	@Override
	public PrestamoDto eliminarPorId(Long id) throws ManagerException{
		// TODO Auto-generated method stub
		if(prestamoRepository.findById(id).orElse(null) == null){
			logger.error("se intento eliminar un prestamo con un id invalido: "+id);
			throw new ManagerException("usted ingreso un id invalido");
		}
		logger.info("Se elimina un prestamo con el id: "+id);
		PrestamoDto prestamoDto = convertirPrestamoDto(prestamoRepository.findById(id).get());
		prestamoRepository.deleteById(id);
		return prestamoDto;
	}
	/**
	 * Obtiene una lista con todos los prestamos de la base de datos.
	 * @return Una lista de libros
	 */
	@Override
	public ArrayList<PrestamoDto> obtenerListaPrestamos() {
		// TODO Auto-generated method stub
		ArrayList<Prestamo> lista = prestamoRepository.findAll();
		ArrayList<PrestamoDto> listaDto = new ArrayList<>();
		for (Prestamo prest : lista) {
			PrestamoDto prestamoDto = convertirPrestamoDto(prest);
			listaDto.add(prestamoDto);
		}
		logger.info("se obtiene la lista prestamos con la cantidad de elementos de: "+listaDto.size());
		return listaDto;
	}
	/**
	 * este metodo se encarga de cambiar el estado
	 * de un libro que esta relacionado a un prestamo
	 * @param el prestamo que contiene el libro
	 */
	public void marcarLibroComoPrestado(Prestamo prestamo) {
		logger.info("marcando libro: "+ prestamo.getLibro().getId()+"como prestado");
		Libro libro = prestamo.getLibro();
		libro.setEstado("Prestado");
		libroRepository.save(libro);
	}
	/**
	 * metodo que prepara un objeto prestamo para registrarlo
	 * correctamente a la base de datos con todos los datos
	 * requeridos
	 * @param idMiembro y idLibro para construir el Prestamo
	 * @return devuelve el prestamo construido para la base de datos
	 * */
	public Prestamo prepararPrestamo(Long idMiembro,Long idLibro) {
		PrestamoDto prestamoDto = new PrestamoDto();
		logger.debug("asignando nuevo id a prestamo");
	    Integer id = obtenerListaPrestamos().size()+1;
	    Long id2 = Long.valueOf(id);
	    prestamoDto.setId(id2);
		prestamoDto.setMiembro(idMiembro);
		prestamoDto.setLibro(idLibro);
		prestamoDto.setEstado("En Prestamo");
		prestamoDto.setFechaYhoraPrestamo(new Date(System.currentTimeMillis()));
		prestamoDto.setFechaYhoraDevolucion(new Date(System.currentTimeMillis()
				+Util.POLITICA_DIAS_PRESTAMO));
		logger.info("se preparo un prestamo correctamente con id: "+id2);
	    return convertirPrestamo(prestamoDto);
	}

	/**
	 * Convierte un prestamo a prestamoDto.
	 * @param prestamo  El prestamo a convertir.
	 * @return Un objeto con los atributos de prestamo.
	 */
	@Override
	public PrestamoDto convertirPrestamoDto(Prestamo prestamo) {
		// TODO Auto-generated method stub
		logger.info("Prestamo conviertiendose en DTO");
			PrestamoDto nuevoDto = new PrestamoDto();
			nuevoDto.setEstado(prestamo.getEstado());
			nuevoDto.setFechaYhoraDevolucion(prestamo.getFechaYhoraDevolucion());
			nuevoDto.setFechaYhoraPrestamo(prestamo.getFechaYhoraPrestamo());
			nuevoDto.setId(prestamo.getId());
			nuevoDto.setMiembro(prestamo.getMiembro().getId());
			nuevoDto.setNombreMiembro(prestamo.getMiembro().getNombre());
			nuevoDto.setLibro(prestamo.getLibro().getId());
			nuevoDto.setTitulolibro(prestamo.getLibro().getTitulo());
            if(prestamo.getDevolucion() != null){
				nuevoDto.setDevolucion(prestamo.getDevolucion().getId());
			}else{
		       nuevoDto.setDevolucion(null);
			}
			return nuevoDto;
          } 
	/**
	 * Convierte un PrestamoDto a Prestamo.
	 * @param prestamoDto  El prestamoDto a convertir.
	 * @return Un prestamoDto.
	 */
	@Override
	public Prestamo convertirPrestamo(PrestamoDto prestamo) {
		// TODO Auto-generated method stub
		logger.info("PrestamoDto conviertiendose en Prestamo");
			Prestamo nuevo = new Prestamo();
			nuevo.setEstado(prestamo.getEstado());
			nuevo.setFechaYhoraDevolucion(prestamo.getFechaYhoraDevolucion());
			nuevo.setFechaYhoraPrestamo(prestamo.getFechaYhoraPrestamo());
			nuevo.setId(prestamo.getId());
			nuevo.setLibro(libroRepository.findById(prestamo.getLibro()).orElse(null));
			nuevo.setMiembro(miembroRepository.findById(prestamo.getMiembro()).orElse(null));
			if(prestamo.getDevolucion() != null) {
			nuevo.setDevolucion(devolucionRepository.findById(prestamo.getDevolucion()).orElse(null));
			}else{
				nuevo.setDevolucion(null);
			}
			return nuevo;
	}

	/*buscar un prestamo en la base de dato con el codigo
	 * coincidente
	 * 
	 * @return devuelve el prestamo encontrado transformado en dto
	 */
	@Override
	public PrestamoDto buscarPrestamoPorId(Long id)throws ManagerException {
		// TODO Auto-generated method stub
		if(prestamoRepository.findById(id).orElse(null) == null){
			logger.error("se busco un prestamo con el id invalido: "+id);
			throw new ManagerException("no se encontro el objeto con ese id");
		}
		Prestamo prestamo = prestamoRepository.findById(id).orElse(null);
		PrestamoDto prestamoDto = convertirPrestamoDto(prestamo);
		logger.info("se busco el prestamo con id: "+id + " correctamente");
		return prestamoDto;
	}
	
	public void mandarCorreoPrestamo(Prestamo prestamo) throws ManagerException{
		try {
		CorreoDto correo = correoService.crearCorreo(correoService.especialParaPrestamo(prestamo),
		prestamo.getMiembro().getCorreoElectronico(),"Prestamo recibido");
		correoService.envioCorreo(correo);
		logger.info("se mando el correo del prestamo con id: "+ prestamo.getId());
		}catch(MessagingException e){
			throw new ManagerException(e.getMessage());
		}
	}
}
