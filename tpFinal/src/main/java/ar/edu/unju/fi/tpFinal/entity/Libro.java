package ar.edu.unju.fi.tpFinal.entity;

import java.util.List;

import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;

import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;

import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;

@Entity
@Table(name = "Libro")
@Order(Ordered.HIGHEST_PRECEDENCE)
public class Libro {
	
	   @Id
	   @GeneratedValue(strategy = GenerationType.IDENTITY)
	   @Column(name = "libro_id")
	   private Long id;
	   @Column(name="titulo")
	   private String titulo;
	   @Column(name="autor")
	   private String autor;
	   @Column(name="isbn")
	   private Integer isbn;
	   @Column(name="numero_inventario")
	   private Integer numeroInventario;
	   @Column(name="estado")
	   private String estado;
	   @OneToMany(mappedBy = "libro", cascade = CascadeType.ALL, orphanRemoval = true)
	   private List<Prestamo> prestamos;
	   
	   public Libro(){
		   
	   }


	public Long getId() {
		return id;
	}


	public void setId(Long id) {
		this.id = id;
	}


	public String getTitulo() {
		return titulo;
	}


	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}


	public String getAutor() {
		return autor;
	}


	public void setAutor(String autor) {
		this.autor = autor;
	}


	public Integer getIsbn() {
		return isbn;
	}


	public void setIsbn(Integer isbn) {
		this.isbn = isbn;
	}


	public Integer getNumeroInventario() {
		return numeroInventario;
	}


	public void setNumeroInventario(Integer numeroInventario) {
		this.numeroInventario = numeroInventario;
	}


	public String getEstado() {
		return estado;
	}


	public void setEstado(String estado) {
		this.estado = estado;
	}


	public List<Prestamo> getPrestamos() {
		return prestamos;
	}


	public void setPrestamos(List<Prestamo> prestamos) {
		this.prestamos = prestamos;
	}



	@Override
	public String toString() {
		return "Libro [id=" + id + ", titulo=" + titulo + ", autor=" + autor + ", isbn=" + isbn + ", numeroInventario="
				+ numeroInventario + ", estado=" + estado + "]";
	}
}
