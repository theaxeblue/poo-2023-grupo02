package ar.edu.unju.fi.tpFinal.service.imp;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import ar.edu.unju.fi.tpFinal.service.AbstractFactory;
import ar.edu.unju.fi.tpFinal.service.ReporteService;

@Service("excelFactory")
public class ReporteExcelFactory implements AbstractFactory{

	@Autowired
	@Qualifier("excel")
	ReporteService reporteExcelService;

	/**
	 * metodo del tipo de fabrica para frabricar el objeto asignado
	 *@return el objeto que frabrica este tipo de frabrica
	 */
	@Override
	public ReporteService crearReporte() {
		// TODO Auto-generated method stub
		return reporteExcelService;
	}

}
