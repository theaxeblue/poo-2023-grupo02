package ar.edu.unju.fi.tpFinal.repository;


import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import ar.edu.unju.fi.tpFinal.entity.Devolucion;


@Repository
public interface DevolucionRepository extends CrudRepository<Devolucion,Long>{

	/**
	 * devuelve todas las devoluciones en una lista
	 */
	public List<Devolucion> findAll();
	

}