package ar.edu.unju.fi.tpFinal.service;

public interface AbstractFactory {

	/**
	 * metodo de la interfaz de la fabrica para crear
	 * los distintos objetos pdf, excel
	 * @return el objeto que tendra los metodos para crear el resumen
	 */
   public ReporteService crearReporte();
}
