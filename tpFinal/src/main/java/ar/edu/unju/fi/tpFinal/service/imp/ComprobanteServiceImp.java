package ar.edu.unju.fi.tpFinal.service.imp;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.itextpdf.io.image.ImageDataFactory;
import com.itextpdf.kernel.colors.ColorConstants;
import com.itextpdf.kernel.geom.PageSize;
import com.itextpdf.kernel.pdf.PdfDocument;
import com.itextpdf.kernel.pdf.PdfWriter;
import com.itextpdf.layout.Document;
import com.itextpdf.layout.element.Cell;
import com.itextpdf.layout.element.Image;
import com.itextpdf.layout.element.Paragraph;
import com.itextpdf.layout.element.Table;
import com.itextpdf.layout.element.Text;
import com.itextpdf.layout.properties.TextAlignment;
import com.itextpdf.layout.properties.UnitValue;
import com.itextpdf.layout.properties.VerticalAlignment;

import ar.edu.unju.fi.tpFinal.exceptions.ManagerException;
import ar.edu.unju.fi.tpFinal.repository.DevolucionRepository;
import ar.edu.unju.fi.tpFinal.service.ComprobanteService;
import ar.edu.unju.fi.tpFinal.service.DevolucionService;
import ar.edu.unju.fi.tpFinal.service.LibroService;
import ar.edu.unju.fi.tpFinal.service.MiembroService;
import ar.edu.unju.fi.tpFinal.service.PrestamoService;
import jakarta.servlet.ServletOutputStream;
import jakarta.servlet.http.HttpServletResponse;

@Service
public class ComprobanteServiceImp implements ComprobanteService{
		// TODO Auto-generated method stub
		public static Logger logger = Logger.getLogger(ComprobanteServiceImp.class);
		
		@Autowired
		PrestamoService prestamoService;
		@Autowired
		DevolucionService devolucionService;
		@Autowired
		DevolucionRepository devolucionRepository;
		@Autowired
		MiembroService miembroService;
		@Autowired
		LibroService libroService;
		/**
	     * metodo que genera el comprobante para la respuesta http
	     * 
	     * @param resumenPrestamo Objeto HttpServletResponse para la respuesta HTTP.
	     * @param id              Identificador del préstamo.
	     */
		@Override
		public void generarComprobante(HttpServletResponse resumenPrestamo,Long id){
			// TODO Auto-generated method stub
			 ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
		      PdfWriter writer = new PdfWriter(byteArrayOutputStream);
		      PdfDocument pdf = new PdfDocument(writer);
		      Document document = new Document(pdf);
		      PageSize pageSize = PageSize.TABLOID;
		      String imagePath = "logoBiblio.png"; 
		      // Carga de la imagen desde el recurso
		      Image image = new Image(ImageDataFactory.create(getClass().getResource("/" + imagePath)))
		              .scaleToFit(100, 100); // Ajusta el tamaño a 100x100 píxeles
		      Table table =  armarDatosDeComprobante(pdf,pageSize,id,image);
		      document.add(table);
		      Table table2 = armarFirmasDeComprobante(image);
		      document.add(table2);
		   // Cerrar el documento
		      document.close();
		      try {
		      ServletOutputStream ops = resumenPrestamo.getOutputStream();
		      // Escribir el contenido en la respuesta
		      byteArrayOutputStream.writeTo(ops);
		      ops.flush();
		      logger.info("se envia la respuesta del documento del comprobante con el prestamo de id: "+id);
		      }catch(IOException e){
		    	  logger.error("ocurrio un error para la respuesta http: "+e.getMessage());
		    	  throw new ManagerException("Error al escribir en la respuesta HTTP"+ e.getMessage());
		      }
		}
		/**
	     * Método que arma la tabla con los datos del comprobante.
	     * 
	     * @param pdf       Documento PDF.
	     * @param pageSize  Tamaño de la página.
	     * @param id        Identificador del préstamo.
	     * @param image     Imagen del logo.
	     * @return Tabla con los datos del comprobante.
	     */
	public Table armarDatosDeComprobante(PdfDocument pdf,PageSize pageSize,Long id,Image image){
		 Table table = new Table(3);
		 DateTimeFormatter formatoFecha = DateTimeFormatter.ofPattern("yyyy/MM MMMM/dd EEEE");
	      pdf.setDefaultPageSize(pageSize);
	      List<String> campos = new ArrayList<>();
			campos.add("INFO MIEMBRO");
			campos.add("INFO. LIBRO");
			campos.add("INFO. PRESTAMO");
	      table.setWidth(UnitValue.createPercentValue(100));
	      float[] columnWidths = {3,3,3}; // Reducir los valores según sea necesario
	      // Crear un párrafo para el título y la imagen
	      Paragraph titleAndImage = new Paragraph()
	              .add(new Text("COMPROBANTE DE PRESTAMO").setFontSize(20)) // Ajusta el tamaño de la fuente según tus necesidades
	              .add("\n") // Agrega un salto de línea entre el título y la imagen
	              .add(image);
	      // Crea una celda del encabezado y agrega la imagen
	      table.addHeaderCell(new Cell(1, campos.size()).add(titleAndImage).setTextAlignment(TextAlignment.CENTER).
	    		  setVerticalAlignment(VerticalAlignment.MIDDLE).setBackgroundColor(ColorConstants.LIGHT_GRAY));
	      // Añade celdas de encabezado
	      for (int i = 0; i < campos.size(); i++) {
	          table.addHeaderCell(new Cell().add(new Paragraph(campos.get(i))).setWidth(columnWidths[i]).setBackgroundColor(ColorConstants.LIGHT_GRAY));
	      }
	      table.addCell(new Cell().add(new Paragraph("Nombre: "+ miembroService.buscarPorId(prestamoService.buscarPrestamoPorId(id).getMiembro()).getNombre())));
	      table.addCell(new Cell().add(new Paragraph("Titulo: "+ libroService.buscarPorId(prestamoService.buscarPrestamoPorId(id).getLibro()).getTitulo())));
	      Date sqlDate = (Date) prestamoService.buscarPrestamoPorId(id).getFechaYhoraPrestamo();
          java.util.Date utilDate = new java.util.Date(sqlDate.getTime());
		  String fechaFormateada = utilDate.toInstant().atZone(ZoneId.systemDefault()).toLocalDate().format(formatoFecha);
	      table.addCell(new Cell().add(new Paragraph("Fecha de Prestamo: "+ fechaFormateada)));
	      table.addCell(new Cell().add(new Paragraph("Correo Electronico: "+ miembroService.buscarPorId(prestamoService.buscarPrestamoPorId(id).getMiembro()).getCorreoElectronico())));
	      table.addCell(new Cell().add(new Paragraph("Autor: "+ libroService.buscarPorId(prestamoService.buscarPrestamoPorId(id).getLibro()).getAutor())));
	      sqlDate = (Date) prestamoService.buscarPrestamoPorId(id).getFechaYhoraDevolucion();
	      utilDate = new java.util.Date(sqlDate.getTime());
		  fechaFormateada = utilDate.toInstant().atZone(ZoneId.systemDefault()).toLocalDate().format(formatoFecha);
	      table.addCell(new Cell().add(new Paragraph("Fecha de Devolucion: "+fechaFormateada)));
	      table.addCell(new Cell().add(new Paragraph("")).setPaddingBottom(100));
	      table.addCell(new Cell().add(new Paragraph("")).setPaddingBottom(100));
	      table.addCell(new Cell().add(new Paragraph("")).setPaddingBottom(100));
	      LocalDate util = LocalDate.now();
		  fechaFormateada = util.format(formatoFecha);
	      Paragraph fechaResumen = new Paragraph()
	              .add("\nFecha del Comprobante: " + fechaFormateada);
	      table.addFooterCell(new Cell(1, campos.size()).add(fechaResumen).setTextAlignment(TextAlignment.CENTER).
	    		  setVerticalAlignment(VerticalAlignment.MIDDLE).setBackgroundColor(ColorConstants.LIGHT_GRAY));
	      logger.info("se genera la parte de la tabla que contiene los datos del comprobante"
	      		+ "del prestamo con id: "+id);
	      return table;
	}
	/**
     * Método que arma la tabla con las firmas del comprobante.
     * 
     * @param image Imagen del logo.
     * @return Tabla con las firmas del comprobante.
     */
	public Table armarFirmasDeComprobante(Image image){
		Table table2 = new Table(2);
	      table2.setWidth(UnitValue.createPercentValue(100));
	      table2.addCell(new Cell().add(new Paragraph("FIRMA DE BIBLIOTECA: ")).setTextAlignment(TextAlignment.CENTER).
	    		  setVerticalAlignment(VerticalAlignment.MIDDLE).setBackgroundColor(ColorConstants.YELLOW));
	      table2.addCell(new Cell().add(new Paragraph("FIRMA DE MIEMBRO: "))).setTextAlignment(TextAlignment.CENTER).
		  setVerticalAlignment(VerticalAlignment.MIDDLE).setBackgroundColor(ColorConstants.YELLOW);
	      table2.addCell(new Cell().add(new Paragraph().add(image))).setTextAlignment(TextAlignment.CENTER).
		  setVerticalAlignment(VerticalAlignment.MIDDLE);
	      Cell guionInferiorCell = new Cell();
	      guionInferiorCell.setPaddingTop(80);
	      guionInferiorCell.add(new Paragraph("---------------------"
	              + "---------------------------------------------")
	              .setTextAlignment(TextAlignment.CENTER)
	              .setVerticalAlignment(VerticalAlignment.BOTTOM));
	      table2.addCell(guionInferiorCell);
	      logger.info("se genera las firmas para la tabla de comprobantes");
	      return table2;
	}
}

