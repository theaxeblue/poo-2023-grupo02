package ar.edu.unju.fi.tpFinal;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import ar.edu.unju.fi.tpFinal.dto.DevolucionDto;
import ar.edu.unju.fi.tpFinal.exceptions.ManagerException;
import ar.edu.unju.fi.tpFinal.service.DevolucionService;
import ar.edu.unju.fi.tpFinal.service.LibroService;
import ar.edu.unju.fi.tpFinal.service.MiembroService;
import ar.edu.unju.fi.tpFinal.service.PrestamoService;

@SpringBootTest
public class DevolucionServiceTest {

	@Autowired
	DevolucionService devolucionService;
	@Autowired
	LibroService libroService;
	
	@Autowired
    MiembroService miembroService;
	
	@Autowired
	PrestamoService prestamoService;
	
	@Test
	@DisplayName("registrar devolucion test")
	void registrarDevolucionTest(){
		//existe un prestamo En devolucion de id 1
		devolucionService.registrarDevolucion(1L);
		//las devoluciones deben ser mayores a 0
		assertTrue(devolucionService.obtenerListaDevolucion().size()>0);
		//la primera devolucion debe tener el id 1
		assertNotNull(devolucionService.buscarDevolucionPorId(1L));
		assertTrue(prestamoService.buscarPrestamoPorId(1L).getEstado().equals("Devuelto"));
		assertTrue(libroService.buscarPorId(prestamoService.buscarPrestamoPorId(1L).getLibro()).getEstado().equals(
				"Disponible"));
		assertTrue(miembroService.buscarPorId(prestamoService.buscarPrestamoPorId(1L).getMiembro()).getDiasSancion()
				== 0);
	}
	
	@Test
	@DisplayName("modificar devolucion test")
	void modificarDevolucionTest() {
		//existe un prestamo de id 2
		DevolucionDto dev = devolucionService.registrarDevolucion(2L);
		Integer cantidad = devolucionService.obtenerListaDevolucion().size();
		//la segunda devolucion creada tiene id 2
		DevolucionDto devolucion2 = new DevolucionDto();
		Long idPrestamoAntiguo = devolucion2.getPrestamo();
		devolucion2 = devolucionService.buscarDevolucionPorId(dev.getId());
		devolucion2.setPrestamo(7L);;
		devolucionService.modificarDevolucion(devolucion2);
		assertNotEquals(devolucionService.buscarDevolucionPorId(devolucion2.getId()).getPrestamo(),idPrestamoAntiguo);
		assertEquals(cantidad,devolucionService.obtenerListaDevolucion().size());
	}
	@Test
	@DisplayName("buscar devolucion test")
	void buscarDevolucionTest() {
		//existe un prestamo de id 4
	    DevolucionDto devolucionDto = devolucionService.registrarDevolucion(4L);
	    // entonces existe una devolucion de id 4
	    assertNotNull(devolucionService.buscarDevolucionPorId(devolucionDto.getId()));
	    //no existe una devolucion con id 55
	    assertThrows(ManagerException.class,()->devolucionService.buscarDevolucionPorId(55L));
	}
	
}
