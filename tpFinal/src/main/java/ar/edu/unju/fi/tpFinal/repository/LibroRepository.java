package ar.edu.unju.fi.tpFinal.repository;

import java.util.ArrayList;
import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import ar.edu.unju.fi.tpFinal.entity.Libro;

@Repository
public interface LibroRepository extends CrudRepository<Libro,Long>{

	/**
     * Busca un libro por su ISBN.
     *
     * @param isbn El ISBN del libro a buscar.
     * @return El libro encontrado o null si no se encuentra.
     */
	public Libro findByIsbn(Integer isbn);
	/**
     * Busca un libro por su autor.
     *
     * @param autor El autor del libro a buscar.
     * @return El libro encontrado o null si no se encuentra.
     */
	public List<Libro> findAllByAutor(String autor);
	/**
     * Obtiene una lista de todos los libros en la biblioteca.
     *
     * @return Una lista de libros.
     */
	public ArrayList<Libro> findAll();
	/**
	 * busca un libro de la base de datos en base al numero de inventario
	 * @param numeroInventario
	 * @return el libro encontrado
	 */
	public Libro findByNumeroInventario(Integer numeroInventario);
}
