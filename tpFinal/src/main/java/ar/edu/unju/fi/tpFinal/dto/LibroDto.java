package ar.edu.unju.fi.tpFinal.dto;

import java.io.Serializable;

import org.springframework.stereotype.Component;
@Component
public class LibroDto implements Serializable{
	   private static final long serialVersionUID = 1L;
	   private Long id;
	   private String titulo;
	   private String autor;
	   private Integer isbn;
	   private Integer numeroInventario;
	   private String estado;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getTitulo() {
		return titulo;
	}
	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}
	public String getAutor() {
		return autor;
	}
	public void setAutor(String autor) {
		this.autor = autor;
	}
	public Integer getIsbn() {
		return isbn;
	}
	public void setIsbn(Integer isbn) {
		this.isbn = isbn;
	}
	public Integer getNumeroInventario() {
		return numeroInventario;
	}
	public void setNumeroInventario(Integer numeroInventario) {
		this.numeroInventario = numeroInventario;
	}
	public String getEstado() {
		return estado;
	}
	public void setEstado(String estado) {
		this.estado = estado;
	}
}
