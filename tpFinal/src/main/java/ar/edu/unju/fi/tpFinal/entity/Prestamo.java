package ar.edu.unju.fi.tpFinal.entity;

import java.sql.Date;

import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToOne;
import jakarta.persistence.Table;

@Entity
@Table(name="Prestamo")
@Order(Ordered.LOWEST_PRECEDENCE)
public class Prestamo {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="prestamo_id")
	private Long id;
	@ManyToOne
	@JoinColumn(name="miembro_id")
	private Miembro miembro;
	@OneToOne
	@JoinColumn(name="libro_id")
	private Libro libro;
	@Column(name="fecha_prestamo")
	private Date fechaYhoraPrestamo;
	@Column(name="fecha_devolucion")
	private Date fechaYhoraDevolucion;
	@Column(name="estado")
	private String estado;
	@OneToOne
	@JoinColumn(name="devolucion_id")
	private Devolucion devolucion;
	
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Miembro getMiembro() {
		return miembro;
	}
	public void setMiembro(Miembro miembro) {
		this.miembro = miembro;
	}
	public Libro getLibro() {
		return libro;
	}
	public void setLibro(Libro libro) {
		this.libro = libro;
	}
	public Date getFechaYhoraPrestamo() {
		return fechaYhoraPrestamo;
	}
	public void setFechaYhoraPrestamo(Date fechaYhoraPrestamo) {
		this.fechaYhoraPrestamo = fechaYhoraPrestamo;
	}
	public Date getFechaYhoraDevolucion() {
		return fechaYhoraDevolucion;
	}
	public void setFechaYhoraDevolucion(Date fechaYhoraDevolucion) {
		this.fechaYhoraDevolucion = fechaYhoraDevolucion;
	}
	public String getEstado() {
		return estado;
	}
	public void setEstado(String estado) {
		this.estado = estado;
	}
	
	public Devolucion getDevolucion() {
		return devolucion;
	}
	public void setDevolucion(Devolucion devolucion) {
		this.devolucion = devolucion;
	}
	@Override
	public String toString() {
		return "Prestamo [id=" + id + ", miembro=" + miembro + ", libro=" + libro + ", fechaYhoraPrestamo="
				+ fechaYhoraPrestamo + ", fechaYhoraDevolucion=" + fechaYhoraDevolucion + ", estado=" + estado + "]";
	}
}
