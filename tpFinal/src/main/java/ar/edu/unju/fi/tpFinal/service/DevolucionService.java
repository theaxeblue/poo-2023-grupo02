package ar.edu.unju.fi.tpFinal.service;

import java.util.List;

import ar.edu.unju.fi.tpFinal.dto.DevolucionDto;
import ar.edu.unju.fi.tpFinal.entity.Devolucion;
import ar.edu.unju.fi.tpFinal.entity.Libro;
import ar.edu.unju.fi.tpFinal.exceptions.ManagerException;

public interface DevolucionService {
	/**
     * Registra una devolución a partir de la información proporcionada en un
     * objeto DevolucionDto.
     *
     * @param id del prestamo para registrar la devolucion
     * @return DevolucionDto con la información de la devolución registrada.
     */
	public DevolucionDto registrarDevolucion(Long id);
	/**
     * Obtiene una lista de objetos DevolucionDto que representan todas las
     * devoluciones registradas.
     *
     * @return Lista de objetos DevolucionDto.
     */
	public List<DevolucionDto> obtenerListaDevolucion();
	/**
     * Modifica la información de una devolución a partir de un objeto
     * DevolucionDto.
     *
     * @param devolucionDto Objeto DevolucionDto con la información actualizada.
     * @return DevolucionDto con la información de la devolución modificada.
     * @throws ManagerException Si se intenta modificar una devolución con un id nulo
     *                          o con un préstamo nulo o inválido.
     */
	public DevolucionDto modificarDevolucion(DevolucionDto devolucionDto) throws ManagerException;
	 /**
     * Busca una devolución por su ID.
     *
     * @param id ID de la devolución a buscar.
     * @return DevolucionDto con la información de la devolución encontrada.
     * @throws ManagerException Si no se encuentra una devolución con el ID
     *                          proporcionado.
     */
	public DevolucionDto buscarDevolucionPorId(Long id) throws ManagerException;
	/**
     * Convierte una devoluciónDto a una devolución.
     *
     * @param devolucionDto Objeto DevolucionDto a convertir.
     * @return Devolucion convertida.
     */
	public Devolucion convertirDevolucion(DevolucionDto devolucionDto);
	/**
     * Convierte una devolución a un objeto DevolucionDto.
     *
     * @param devolucion Devolución a convertir.
     * @return DevolucionDto con la información de la devolución.
     */
	public DevolucionDto convertirDevolucionDto(Devolucion devolucion);
	/**
     * Marca un libro como disponible cambiando su estado a "Disponible".
     *
     * @param libro Libro a marcar como disponible.
     */
	public void marcarLibroComoDisponible(Libro libro);
	/**
     * Aplica la sanción correspondiente a un miembro según la diferencia entre la
     * fecha de devolución y la fecha límite de devolución del préstamo.
     *
     * @param devolucion Devolución a partir de la cual se calcula la sanción.
     */
	public void sancionarMiembro(Devolucion devolucion);
	/**
     * Marca un préstamo como devuelto cambiando su estado a "Devuelto".
     *
     * @param devolucion Devolución asociada al préstamo a marcar como devuelto.
     */
	public void marcarPrestamoComoDevuelto(Devolucion devolucion);
	/**
     * Prepara una devolución a partir de un objeto DevolucionDto.
     *
     * @param devolucionDto Objeto DevolucionDto con la información de la devolución.
     * @return Devolucion preparada.
     */
	public Devolucion prepararDevolucion(Long id);
	
}
