package ar.edu.unju.fi.tpFinal.repository;

import java.util.ArrayList;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import ar.edu.unju.fi.tpFinal.entity.Miembro;

@Repository
public interface MiembroRepository extends CrudRepository<Miembro,Long>{
	 /**
     * Obtiene una lista de todos los miembros en la base de datos.
     *
     * @return Una lista de miembros.
     */
	 public ArrayList<Miembro> findAll();
	     /**
	     * Busca un miembro por su código.
	     *
	     * @param codigo El código del miembro a buscar.
	     * @return El miembro encontrado o null si no se encuentra.
	     */
     public Miembro findByNumeroMiembro(Integer numeroMiembro);
         /**
	     * borra un miembro por id.
	     *
	     * @param id del miembro a buscar.
	     */
	 public void deleteById(Long id);
	 /**
	     * borra un miembro por NumeroMiembro.
	     *
	     * @param numeroMiembro del miembro a buscar.
	     */
	 public void deleteByNumeroMiembro(Integer numeroMiembro);
	     /**
	     * busca un miembro por CorreoElectronico.
	     *
	     * @param el correo del miembro a buscar
	     * @return el Mimbro devuelto
	     */
	 public Miembro findByCorreoElectronico(String correo);
}
