package ar.edu.unju.fi.tpFinal;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import ar.edu.unju.fi.tpFinal.dto.CorreoDto;
import ar.edu.unju.fi.tpFinal.service.CorreoService;
import ar.edu.unju.fi.tpFinal.service.LibroService;
import ar.edu.unju.fi.tpFinal.service.MiembroService;
import ar.edu.unju.fi.tpFinal.service.PrestamoService;
import jakarta.mail.MessagingException;

@SpringBootTest
public class CorreoServiceTest {
	@Autowired
	CorreoService correoService;
	@Autowired
	LibroService libroService;
	@Autowired
	MiembroService miembroService;
	@Autowired
	PrestamoService prestamoService;
	/**
	 * prueba de funcionalidad del envio de correo html en base
	 * a un prestamo creado a
	 * un correo electronico de un miembro.
	 */
	@Test
	@DisplayName("Envio de correo")
	void envioDeCorreo() throws MessagingException {
		//ya existe un prestamo con el id 1
		String cuerpo = correoService.especialParaPrestamo(prestamoService.convertirPrestamo(prestamoService.buscarPrestamoPorId(1L)));
		String asunto ="Prestamo de libro";
		assertEquals("poo2023biblioteca@gmail.com",prestamoService.convertirPrestamo(prestamoService.buscarPrestamoPorId(1L)).getMiembro().getCorreoElectronico());
		// se crea un correo sin el @ en el destinatario
		CorreoDto correoDto = correoService.crearCorreo("nuevoCuerpo", "milton.com", "nuevoAsunto");
		assertThrows(MessagingException.class,()-> correoService.construirCorreo(correoDto));
		correoService.envioCorreo(correoService.crearCorreo(cuerpo,prestamoService.convertirPrestamo(prestamoService.buscarPrestamoPorId(1L)).getMiembro().getCorreoElectronico() 
				, asunto));
		assertNotEquals(correoService.especialParaPrestamo(prestamoService.convertirPrestamo(prestamoService.buscarPrestamoPorId(1L))),"");
	}
}
