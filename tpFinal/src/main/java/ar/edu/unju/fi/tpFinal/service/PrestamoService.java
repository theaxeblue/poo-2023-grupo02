package ar.edu.unju.fi.tpFinal.service;

import java.util.ArrayList;

import ar.edu.unju.fi.tpFinal.dto.PrestamoDto;
import ar.edu.unju.fi.tpFinal.entity.Prestamo;
import ar.edu.unju.fi.tpFinal.exceptions.ManagerException;
import jakarta.mail.MessagingException;

public interface  PrestamoService {
	/**
     * Registra un prestamo en la biblioteca.
     *
     * @param prestamo El prestamo a ser guardado.
     */
	public PrestamoDto registrarPrestamo(Long idMiembro,Long idLibro) throws ManagerException, MessagingException;
	/**
     * Modifica un prestamo existente en la biblioteca.
     *
     * @param prestamo El prestamo a ser modificado.
     */
	public PrestamoDto modificarPrestamo(PrestamoDto prestamoDto);
	
	
	 /**
     * Elimina un prestamo registrado en la biblioteca.
     *
     * @param prestamo El prestamo a ser eliminado.
     */
	public PrestamoDto eliminarPorId(Long id);
	
	/*buscar un prestamo en la base de dato con el codigo
	 * coincidente
	 * 
	 * @return devuelve el prestamo encontrado transformado en dto
	 */
	public PrestamoDto buscarPrestamoPorId(Long id);
	/**
     * Obtiene una lista de todos los prestamos en la biblioteca.
     *
     * @return Una lista de libros.
     */
	public ArrayList<PrestamoDto> obtenerListaPrestamos();
	

	/**
	 * convierte un prestamo a PrestamoDto
	 * @param el prestamo a convertir
	 * @return un objeto con los atributos de prestamoDto
	 */
	public PrestamoDto convertirPrestamoDto(Prestamo prestamo);
	
	
	/**
	 * convierte un prestamoDto a prestamo
	 * @param prestamoDto a convertir
	 * @return un objeto con los atributos de prestamoDto
	 */
	public Prestamo convertirPrestamo(PrestamoDto prestamoDto);

	/**
	 * marca el libro asociado a un prestamo como Prestado
	 * @param prestamo para marcar el libro
	 */
	public void marcarLibroComoPrestado(Prestamo prestamo);
	/**
	 * manda un correo en base a un prestamo
	 * @param prestamo tiene lo que se necesita para el correo
	 */
	public void mandarCorreoPrestamo(Prestamo prestamo) throws MessagingException;
	/**
	 * metodo que prepara un objeto prestamo para registrarlo
	 * correctamente a la base de datos con todos los datos
	 * requeridos
	 * @param idMiembro y idLibro para construir el Prestamo
	 * @return devuelve el prestamo construido para la base de datos
	 * */
	public Prestamo prepararPrestamo(Long idMiembro,Long idLibro);
}
