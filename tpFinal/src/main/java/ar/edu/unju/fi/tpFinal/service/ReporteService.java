package ar.edu.unju.fi.tpFinal.service;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Date;
import java.util.List;
import ar.edu.unju.fi.tpFinal.dto.PrestamoDto;
import jakarta.servlet.http.HttpServletResponse;

public interface ReporteService {
	/**
     * Obtiene una lista de préstamos filtrados por un rango de fechas.
     *
     * @param fechaInicio Fecha de inicio del rango.
     * @param fechaFin    Fecha de fin del rango.
     * @return Lista de objetos que cumplen con los criterios de filtrado.
     */
	public List<PrestamoDto> obtenerPrestamosFiltrados(Date fechaInicio, Date fechaFin);

	/**
     * Genera un resumen de préstamos y lo devuelve como respuesta HTTP.
     *
     * @param resumenPrestamo Objeto para escribir la respuesta.
     * @param fechaInicio     Fecha de inicio del rango.
     * @param fechaFin        Fecha de fin del rango.
     */
	public void generarResumen(HttpServletResponse resumenPrestamo, Date fechaInicio, Date fechaFin) throws FileNotFoundException, IOException;
	 /**
     * Obtiene una lista de atributos de un objeto prestamoDto.
     *
     * @param prestamoDto Objeto del cual se obtendrán los atributos.
     * @return Lista de objetos que representan los atributos del préstamo.
     */
	public List<Object> obtenerAtributos(PrestamoDto prestamoDto);
	
	/**
     * Obtiene una lista de nombres de campos para la creación de tablas.
     *
     * @return Lista de cadenas que representan los nombres de los campos.
     */
	public List<String> obtenerListaCampos();
	/**
     * Crea una tabla a partir de una lista de préstamos y campos específicos.
     *
     * @param listaFiltrada Lista de préstamos a incluir en la tabla.
     * @param campos         Lista de nombres de campos para la tabla.
     * @return Objeto que representa la tabla generada.
     */
	public Object crearTabla(List<PrestamoDto> listaFiltrada,List<String> campos);
}
