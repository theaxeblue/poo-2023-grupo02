package ar.edu.unju.fi.tpFinal.dto;

import java.io.Serializable;
import java.sql.Date;

import org.springframework.stereotype.Component;

@Component
public class PrestamoDto implements Serializable {
	private static final long serialVersionUID = 1L;
	private Long id;
	private Long miembro;
	private Long libro;
	private Long devolucion;
	private Date fechaYhoraPrestamo;
	private Date fechaYhoraDevolucion;
	private String nombreMiembro;
	private String tituloLibro;
	private String estado;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getMiembro() {
		return miembro;
	}
	public void setMiembro(Long miembro) {
		this.miembro = miembro;
	}
	public Long getLibro() {
		return libro;
	}
	public void setLibro(Long libro) {
		this.libro = libro;
	}
	public Date getFechaYhoraPrestamo() {
		return fechaYhoraPrestamo;
	}
	public void setFechaYhoraPrestamo(Date fechaYhoraPrestamo) {
		this.fechaYhoraPrestamo = fechaYhoraPrestamo;
	}
	public Date getFechaYhoraDevolucion() {
		return fechaYhoraDevolucion;
	}
	public void setFechaYhoraDevolucion(Date fechaYhoraDevolucion) {
		this.fechaYhoraDevolucion = fechaYhoraDevolucion;
	}
	public String getNombreMiembro() {
		return nombreMiembro;
	}
	public void setNombreMiembro(String nombreMiembro) {
		this.nombreMiembro = nombreMiembro;
	}
	public String getTitulolibro() {
		return tituloLibro;
	}
	public void setTitulolibro(String tituloLibro) {
		this.tituloLibro = tituloLibro;
	}
	public String getEstado() {
		return estado;
	}
	public void setEstado(String estado) {
		this.estado = estado;
	}
	public Long getDevolucion() {
		return devolucion;
	}
	public void setDevolucion(Long devolucion) {
		this.devolucion = devolucion;
	}
}
