package ar.edu.unju.fi.tpFinal.entity;

import java.util.List;

import org.springframework.core.*;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import jakarta.persistence.DiscriminatorColumn;
import jakarta.persistence.Inheritance;
import jakarta.persistence.InheritanceType;
import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;


@Entity
@Table(name = "Miembro")
@Inheritance( strategy = InheritanceType.SINGLE_TABLE )
@DiscriminatorColumn(name = "tipo")
@Order(Ordered.HIGHEST_PRECEDENCE)
public class Miembro {
	   @Id
	   @GeneratedValue(strategy = GenerationType.IDENTITY)
	   @Column(name = "miembro_id")
	   private Long id;
	   @Column(name="nombre")
	   private String nombre;
	   @Column(name="numero_miembro")
	   private Integer numeroMiembro;
	   @Column(name="correo_electronico")
	   private String correoElectronico;
	   @Column(name="telefono")
	   private Integer telefono;
	   @Column(name="dias_sancion")
	   private Integer diasSancion;
	   @OneToMany(mappedBy = "miembro", cascade = CascadeType.ALL, orphanRemoval = true)
	   private List<Prestamo> prestamos;
	   public Miembro() {
	   }


	public Long getId() {
		return id;
	}


	public void setId(Long id) {
		this.id = id;
	}


	public String getNombre() {
		return nombre;
	}


	public void setNombre(String nombre) {
		this.nombre = nombre;
	}


	public Integer getNumeroMiembro() {
		return numeroMiembro;
	}


	public void setNumeroMiembro(Integer numeroMiembro) {
		this.numeroMiembro = numeroMiembro;
	}


	public String getCorreoElectronico() {
		return correoElectronico;
	}


	public void setCorreoElectronico(String correoElectronico) {
		this.correoElectronico = correoElectronico;
	}


	public Integer getTelefono() {
		return telefono;
	}


	public void setTelefono(Integer telefono) {
		this.telefono = telefono;
	}

	
	public List<Prestamo> getPrestamos() {
		return prestamos;
	}


	public void setPrestamos(List<Prestamo> prestamos) {
		this.prestamos = prestamos;
	}


	public Integer getDiasSancion() {
		return diasSancion;
	}


	public void setDiasSancion(Integer diasSancion) {
		this.diasSancion = diasSancion;
	}


	@Override
	public String toString() {
		return "Miembro [id=" + id + ", nombre=" + nombre + ", numeroMiembro=" + numeroMiembro + ", correoElectronico="
				+ correoElectronico + ", telefono=" + telefono + "]";
	}


}
