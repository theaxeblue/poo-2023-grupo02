package ar.edu.unju.fi.tpFinal.exceptions;

public class ManagerException extends RuntimeException{
    private static final long serialVersionUID=0;
	private Throwable cause;
	
	/**
     * Constructor de la clase ManagerException.
     *
     * @param message Mensaje descriptivo que indica la causa del error.
     */
	public ManagerException(String message){
		super(message);
	}
	/**
     *
     * @return La causa de esta excepción.
     */
	@Override
	public Throwable getCause() {
		return this.cause;
	}
}
