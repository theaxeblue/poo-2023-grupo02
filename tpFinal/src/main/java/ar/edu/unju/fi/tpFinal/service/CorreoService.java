package ar.edu.unju.fi.tpFinal.service;

import ar.edu.unju.fi.tpFinal.dto.CorreoDto;
import ar.edu.unju.fi.tpFinal.entity.Prestamo;
import jakarta.mail.MessagingException;
import jakarta.mail.internet.MimeMessage;

public interface CorreoService {
	/**
     * Construye un mensaje de correo electrónico a partir de un objeto CorreoDto.
     *
     * @param correo Objeto CorreoDto con la información del correo electrónico.
     * @return MimeMessage con el mensaje de correo electrónico construido.
     * @throws MessagingException Si ocurre un error al construir el mensaje.
     */
   public MimeMessage construirCorreo(CorreoDto correo) throws MessagingException;
   /**
    * Envía un correo electrónico a partir de un objeto CorreoDto.
    *
    * @param correo Objeto CorreoDto con la información del correo electrónico a enviar.
    * @throws MessagingException Si ocurre un error al enviar el correo.
    */
   public void envioCorreo(CorreoDto correo) throws MessagingException;
   /**
    * Crea un objeto CorreoDto a partir de los parámetros proporcionados.
    *
    * @param cuerpo       Cuerpo del correo electrónico.
    * @param destinatario Dirección de correo electrónico del destinatario.
    * @param asunto       Asunto del correo electrónico.
    * @return CorreoDto con la información del correo creado.
    */
   public CorreoDto crearCorreo(String cuerpo, String destinatario, String asunto);
   /**
    * Genera un mensaje especial para notificar sobre un préstamo.
    *
    * @param prestamo Objeto Prestamo para el cual se genera el mensaje especial.
    * @return Mensaje especial para el préstamo.
    */
   public String especialParaPrestamo(Prestamo prestamo);
}
