<h1 align="center"> Gestión de  Biblioteca </h1>

Este proyecto consiste en una aplicación de gestión de biblioteca desarrollada en Java utilizando Java Persistence API (JPA). El sistema permite a los bibliotecarios llevar un registro de los libros en la biblioteca, los préstamos realizados a los miembros y la información básica de los miembros.


## Tecnologías utilizadas

- Java: El lenguaje de programación principal utilizado para desarrollar la aplicación.
versión 17

- Java Persistence API (JPA): Utilizado para mapear objetos Java a entidades de base de datos y gestionar la persistencia de datos.

- Bases de datos(MySQL), 

-Frameworks(Spring Tool Suite 4.1.5 y Spring Tool Suite 4.15.3).]



## Autores

- Chaile V. Andrés LU: 3346
- Delgado Milton LU: 4565

