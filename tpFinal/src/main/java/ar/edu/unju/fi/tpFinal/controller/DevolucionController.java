package ar.edu.unju.fi.tpFinal.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ar.edu.unju.fi.tpFinal.dto.DevolucionDto;
import ar.edu.unju.fi.tpFinal.exceptions.ManagerException;
import ar.edu.unju.fi.tpFinal.service.DevolucionService;

@RestController
@RequestMapping("/biblioteca/v1/devolucion")
public class DevolucionController {
	private final Logger logger = LoggerFactory.getLogger(DevolucionController.class);
	@Autowired
	DevolucionService devolucionService;
	/**
     * Registra una nueva devolución.
     * @param devolucionDto Objeto DevolucionDto con la información de la nueva devolución.
     * @return ResponseEntity con el resultado de la operación y mensajes asociados.
     */
	@PostMapping("/registrarDevolucion/{id}")
	public ResponseEntity<?> registrarDevolucion(@PathVariable Long id){
		Map<String, Object> response = new HashMap<String, Object>();
		try {
			logger.info("inicia el registro de la devolucion del prestamo"
					+ "con id: "+ id);
		    response.put("Devolucion", devolucionService.registrarDevolucion(id));
		    response.put("Mensaje", "se registro la devolucion correctamente");
		}catch(ManagerException e){
			response.put("Mensaje", "Ocurrio un error al intentar registrar la devolucion");
			response.put("Error", e.getMessage());
			return new ResponseEntity<Map<String,Object>>(response,HttpStatus.INTERNAL_SERVER_ERROR);
		}
		logger.info("se retorna la respuesta al registro de la devoluciones"
				+ "con el prestamo id: "+ id);
		return new ResponseEntity<Map<String,Object>>(response,HttpStatus.CREATED);
	}
	/**
     * Obtiene la lista de devoluciones.
     *
     * @return ResponseEntity con la lista de devoluciones y mensajes asociados.
     */
	@GetMapping("/devoluciones")
	public ResponseEntity<?> obtenerDevoluciones(){
		Map<String, Object> response = new HashMap<String, Object>();
		List<DevolucionDto> listaDto = new ArrayList<DevolucionDto>();
		try {
			logger.info("inicia la consulta de devoluciones");
			listaDto = devolucionService.obtenerListaDevolucion();
		    response.put("Devoluciones", listaDto);
		    response.put("Mensaje", "objetos consultados correctamente");
		}catch(DataAccessException e) {
			response.put("Mensaje", "Ocurrio un error al intentar consultar las devoluciones");
			response.put("Error", e.getCause().getMessage());
			return new ResponseEntity<Map<String,Object>>(response,HttpStatus.INTERNAL_SERVER_ERROR);
		}
		logger.info("se retorna la respuesta de: " + listaDto.size()+" devoluciones");
		return new ResponseEntity<Map<String,Object>>(response,HttpStatus.OK);
	}
	/**
     * Modifica una devolución existente.
     *
     * @param devolucionDto Objeto DevolucionDto con la información de la devolución a modificar.
     * @return ResponseEntity con el resultado de la operación y mensajes asociados.
     */
	@PutMapping("/modificarDevolucion")
	public ResponseEntity<?> modificarDevolucion(@RequestBody DevolucionDto devolucionDto){
		Map<String, Object> response = new HashMap<String, Object>();
		try {
			logger.info("iniciando la modificacion de devolucion con el id: "+ devolucionDto.getId());
		    response.put("Devolucion", devolucionService.modificarDevolucion(devolucionDto));
		    response.put("Mensaje", "objeto modificado correctamente");
		}catch(ManagerException e) {
			response.put("Mensaje", "Ocurrio un error al intentar consultar las devoluciones");
			response.put("Error", e.getMessage());
			return new ResponseEntity<Map<String,Object>>(response,HttpStatus.INTERNAL_SERVER_ERROR);
		}
		logger.info("se retorna la respuesta sin problemas a la modificacion"
				+ "de la devolucion con el id: "+ devolucionDto.getId());
		return new ResponseEntity<Map<String,Object>>(response,HttpStatus.OK);
	}
	/**
     * Busca una devolución por su ID.
     *
     * @param id ID de la devolución a buscar.
     * @return ResponseEntity con el resultado de la operación y mensajes asociados.
     */
	@GetMapping("/buscarDevolucion/{id}")
	public ResponseEntity<?> buscarDevolucion(@PathVariable Long id){
		Map<String, Object> response = new HashMap<String, Object>();
		  try {
			logger.info("se inicia la obtencion de la devolucion con el id: "+ id);
		    response.put("Devolucion", devolucionService.buscarDevolucionPorId(id));
		    response.put("Mensaje", "objeto encontrado Correctamente");
	       }catch(ManagerException e) {
			response.put("Mensaje", "Ocurrio un error al intentar buscar el objeto");
			response.put("Error", e.getMessage());
			return new ResponseEntity<Map<String,Object>>(response,HttpStatus.INTERNAL_SERVER_ERROR);    
	       }
		logger.info("se retorna la respuesta sin problemas a la obtencion"
				+ "de la devolucion con el id: "+ id);
		return new ResponseEntity<Map<String,Object>>(response,HttpStatus.OK);
	}
}
