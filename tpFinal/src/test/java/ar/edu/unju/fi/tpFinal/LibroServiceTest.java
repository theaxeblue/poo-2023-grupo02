package ar.edu.unju.fi.tpFinal;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import ar.edu.unju.fi.tpFinal.dto.LibroDto;
import ar.edu.unju.fi.tpFinal.exceptions.ManagerException;
import ar.edu.unju.fi.tpFinal.service.LibroService;

@SpringBootTest
public class LibroServiceTest {

	@Autowired
	LibroService libroService;
	
	/**
	 * Prueba la funcionalidad de guardar y crear un libro.
	 * Se verifica que un libro se guarda correctamente y que se puede encontrar por su ISBN.
	 */
	@Test
	@DisplayName("Guardar y crear libro")
	void guardarCrearLibroTest(){
		Integer cantidad = libroService.obtenerListaLibros().size();
		LibroDto libroNuevo = new LibroDto();
		libroNuevo.setIsbn(1);
		libroNuevo.setNumeroInventario(1);
		libroService.guardarLibro(libroNuevo);
		assertNotEquals(null,libroService.buscarPorIsbn(1));
		assertTrue(libroService.obtenerListaLibros().size() > cantidad);
		cantidad = libroService.obtenerListaLibros().size();
		assertNotNull(libroService.buscarPorIsbn(1));
		LibroDto libroNuevo2 = new LibroDto();
		libroNuevo.setIsbn(4);
		libroNuevo.setNumeroInventario(4);
		libroService.guardarLibro(libroNuevo2);
		assertFalse(libroService.obtenerListaLibros().size() < cantidad);
		}
	/**
	 * Prueba la funcionalidad de eliminar un libro.
	 * Se verifica que un libro se elimina correctamente y que ya no se puede encontrar por el autor.
	 * se utiliza la eliminacion mediante identificadores.
	 */
	@Test
	@DisplayName("eliminar libro")
	void eliminarLibroTest(){
		//existe un libro con isbn 100 en la base de datos
		Integer cantidad = libroService.obtenerListaLibros().size();
		libroService.eliminarLibroPorIsbn(100);
		assertThrows(ManagerException.class,()->libroService.buscarPorIsbn(100));
		assertTrue(cantidad > libroService.obtenerListaLibros().size());
	}
	/**
	 * Prueba la funcionalidad de editar un libro.
	 * Se verifica que un libro se puede editar correctamente y que los cambios se reflejan en la base de datos.
	 */
	@Test
	@DisplayName("editar libro")
	void editarLibroTest(){
		int cantidad = libroService.obtenerListaLibros().size();
		// existe un libro con isbn 101 con el id 2 en la base de datos
		assertNotNull(libroService.buscarPorIsbn(101));
		LibroDto libroNuevo = new LibroDto();
		String tituloActual = libroService.buscarPorIsbn(101).getTitulo();
		libroNuevo.setTitulo("nuevo titulo");
		libroNuevo.setId(libroService.buscarPorIsbn(101).getId());
		libroNuevo.setIsbn(libroService.buscarPorIsbn(101).getIsbn());
		libroNuevo.setNumeroInventario(libroService.buscarPorIsbn(101).getNumeroInventario());
		libroService.modificarLibro(libroNuevo);
		assertEquals(cantidad,libroService.obtenerListaLibros().size());
		assertNotEquals(libroService.buscarPorIsbn(101).getTitulo(),tituloActual);
		assertNotNull(libroService.buscarPorIsbn(101));
	}
	/**
	 * Prueba la funcionalidad de buscar un libro por diferentes criterios.
	 * Se verifica que se pueden buscar libros por ISBN, título y autor.
	 */
	@Test
	@DisplayName("Buscar libro")
	void buscarLibroTest(){
		//existe un libro con el isbn 102 en la bd
		assertNotNull(libroService.buscarPorIsbn(102));
		//existe el mismo libro con el id 2
		assertEquals(libroService.buscarPorIsbn(102).getAutor(),
				libroService.buscarPorIsbn(102).getAutor());
		LibroDto libroNuevo = new LibroDto();
		libroNuevo.setIsbn(2);
		libroNuevo.setNumeroInventario(2);
		libroNuevo.setTitulo("fantastico");
		libroService.guardarLibro(libroNuevo);
		assertNotNull(libroService.buscarPorIsbn(2));
		//no existe el libro con el isbn 55
		assertThrows(ManagerException.class,()->libroService.buscarPorIsbn(55));
		assertNotNull(libroService.buscarPorIsbn(2));
		//J.R.R. Tolkien es autor de mas de un libro
		assertTrue(libroService.buscarPorAutor("J.R.R. Tolkien").size() > 0);
		assertFalse(libroService.buscarPorAutor("a").size() != 0);
	}
	/**
	 * metodo test que controla las validaciones del metodo
	 * guardarLibro() para que no existan libros con el
	 * mismo isbn
	 */
	@Test
	@DisplayName("validacion de AgregarLibro")
	void validacionAgregarLibroTest(){
		LibroDto libroNuevo = new LibroDto();
		// ya existe un libro con isbn 102 en la bd
		libroNuevo.setIsbn(102);
		libroNuevo.setNumeroInventario(3);
		//ya existe un libro con el numero de inventario de 1003 en la bd
		LibroDto libroNuevo2 = new LibroDto();
		libroNuevo2.setIsbn(3);
		libroNuevo2.setNumeroInventario(1003);
		assertThrows(ManagerException.class,()->libroService.guardarLibro(libroNuevo));
		assertThrows(ManagerException.class,()->libroService.guardarLibro(libroNuevo2));
		
	}
}
