package ar.edu.unju.fi.tpFinal.service.imp;



import ar.edu.unju.fi.tpFinal.dto.CorreoDto;
import ar.edu.unju.fi.tpFinal.entity.Prestamo;
import ar.edu.unju.fi.tpFinal.service.CorreoService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import jakarta.mail.MessagingException;
import jakarta.mail.internet.*;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

@Service
public class CorreoServiceImp implements CorreoService {

	@Autowired
	JavaMailSender mailSender;
	@Autowired
	PrestamoServiceImp prestamoService;
	public static Logger logger = Logger.getLogger(PrestamoServiceImp.class);
	/**
	 * Se construye correo electronico a enviar.
	 * @param Correo electronico.
	 * @return El mensaje del correo electronico.
	 */
	@Override
	public MimeMessage construirCorreo(CorreoDto correo) throws MessagingException{
		  		MimeMessage message = mailSender.createMimeMessage();
				logger.debug("Construyendo mensaje a enviar");
				message.setSubject(correo.getAsunto());
				MimeMessageHelper helper = new MimeMessageHelper(message, true);
				 if (!correo.getDestinatario().contains("@")) {
				        throw new MessagingException("El correo destinatario no es válido.");
				    }
		        helper.setTo(correo.getDestinatario());
			    helper.setText(correo.getCuerpo(),true);
		return message;
	}

	/**
	 * Se envia correo electronico construido.
	 * @param Correo electronico para enviar.
	 */
	@Override
	public void envioCorreo(CorreoDto correo) throws MessagingException{
	        logger.debug("enviando mensaje");
	        mailSender.send(construirCorreo(correo));
	        logger.info("Mensaje Enviado");
	}

	/**
	 * Se crea el correo electrónico que se va enviar.
	 * @param destinatario La dirección de correo electronico de destino.
	 * @param asunto El asunto del correo electrónico.
	 * @param Cuerpo El contenido del correo electrónico
	 * return nuevoCorreo El correo electrónico creado.
	 */
	@Override
	public CorreoDto crearCorreo(String cuerpo, String destinatario, String asunto) {
	    CorreoDto nuevoCorreo = new CorreoDto();
	    nuevoCorreo.setAsunto(asunto);
	    nuevoCorreo.setCuerpo(cuerpo);
	    nuevoCorreo.setDestinatario(destinatario);
	    logger.info("Se crea Correo electronico para enviar");
		return nuevoCorreo;
	}

	/**
	 * Se personaliza el mensaje del correo electrónico creado, con etiquetas de html.
	 * @param prestamo El prestamo de un libro realizado a un miembro
	 * @return Contenido de correo electronico estilizado con html.
	 */
	@Override
	public String especialParaPrestamo(Prestamo prestamo) {
		logger.info("Se crea el contenido del correo electronico");
		return "<!DOCTYPE html>\r\n"+ "<html lang=\"en\">\r\n"+ "<head>\r\n"
		  		+ "    <meta charset=\"UTF-8\">\r\n"+ "    <title>envio de correo</title>\r\n"+ "</head>\r\n"
		  		+ "<body>\r\n"+ "    <h1>¡Hola "+ prestamo.getMiembro().getNombre()+"!</h1>\r\n"
		  		+ "    <p> Usted recibio el prestamo de un libro, a continuacion los datos relevantes del prestamo: </p>\r\n"+ " \r\n"
		  		+ "    <p> Nombre del libro: " + prestamo.getLibro().getTitulo()+ " </p>\r\n"
		  	    + "    <p> Numero de Inventario: " + prestamo.getLibro().getNumeroInventario()+ " </p>\r\n"
		  		+ "    <p> Numero de miembro: " + prestamo.getMiembro().getNumeroMiembro() + " </p>\r\n"
		  		+ "    <p> Fecha de Prestamo: " + prestamo.getFechaYhoraPrestamo()+ " </p>\r\n"
		  		+ "    <p> Fecha de Devolucion: " + prestamo.getFechaYhoraDevolucion()+ " </p>\r\n"	
		  		+ "    <img src=\"http://imgfz.com/i/eykdFYW.png\" alt=\"LogoBiblioteca\" alt=\"LogoBiblioteca\" width=\"300\" height=\"300\">\r\n"+ "\r\n"
		  		+ "</body>\r\n"+ "</html>";
	}
}
