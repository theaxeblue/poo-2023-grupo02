package ar.edu.unju.fi.tpFinal.dto;

import java.io.Serializable;

import org.springframework.stereotype.Component;

@Component
public class MiembroDto implements Serializable{
	   private static final long serialVersionUID = 1L;
	   private Long id;
	   private String nombre;
	   private Integer numeroMiembro;
	   private String correoElectronico;
	   private Integer telefono;
	   private Integer diasSancion;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public Integer getNumeroMiembro() {
		return numeroMiembro;
	}
	public void setNumeroMiembro(Integer numeroMiembro) {
		this.numeroMiembro = numeroMiembro;
	}
	public String getCorreoElectronico() {
		return correoElectronico;
	}
	public void setCorreoElectronico(String correoElectronico) {
		this.correoElectronico = correoElectronico;
	}
	public Integer getTelefono() {
		return telefono;
	}
	public void setTelefono(Integer telefono) {
		this.telefono = telefono;
	}
	public Integer getDiasSancion() {
		return diasSancion;
	}
	public void setDiasSancion(Integer diasSancion) {
		this.diasSancion = diasSancion;
	}
}
