package ar.edu.unju.fi.tpFinal;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import ar.edu.unju.fi.tpFinal.dto.AlumnoDto;
import ar.edu.unju.fi.tpFinal.dto.DocenteDto;
import ar.edu.unju.fi.tpFinal.dto.MiembroDto;
import ar.edu.unju.fi.tpFinal.exceptions.ManagerException;
import ar.edu.unju.fi.tpFinal.service.MiembroService;

@SpringBootTest
public class MiembroServiceTest {

	@Autowired
	MiembroService miembroService;
	/**
	 * Prueba la funcionalidad de guardar y crear un miembro.
	 * Se verifica que un miembro se guarda correctamente y que se puede encontrar por su código.
	 */
	@Test
	@DisplayName("Guardar Miembro")
	void guardarCrearMiembroTest(){
		AlumnoDto miembroNuevo = new AlumnoDto();
		miembroNuevo.setNumeroMiembro(1);
		miembroNuevo.setCorreoElectronico("correo3@gmail.com");
		Integer cantidad = miembroService.obtenerListaMiembros().size();
		miembroService.guardarMiembro(miembroNuevo);
		MiembroDto miembroEncontrado = miembroService.buscarPorNumeroMiembro(1);
		assertNotNull(miembroEncontrado);
		assertTrue(miembroService.obtenerListaMiembros().size() > cantidad);
		DocenteDto miembroNuevo2 = new DocenteDto();
		miembroNuevo2.setNumeroMiembro(2);
		miembroNuevo2.setCorreoElectronico("correo2@gmail.com");
		miembroNuevo2.setLegajo(123);
		miembroService.guardarMiembro(miembroNuevo2);
		DocenteDto miembroEncontrado2 = (DocenteDto)miembroService.buscarPorNumeroMiembro(2);
		assertNotNull(miembroEncontrado2);
		assertEquals(miembroEncontrado2.getLegajo(),123);
		miembroService.eliminarPorNumeroMiembro(1);
		miembroService.eliminarPorNumeroMiembro(2);
		assertTrue(miembroService.obtenerListaMiembros().size() == cantidad);
	}	
	/**
	 * Prueba la funcionalidad de eliminar un miembro.
	 * Se verifica que un miembro se elimina correctamente y que ya no se puede encontrar por su código.
	 */
	@Test
	@DisplayName("eliminar miembro")
	void eliminarMiembroTest(){
		Integer cantidadInicial = miembroService.obtenerListaMiembros().size();
		//existe un miembro con el numero de miembro 114 en la bd
		miembroService.eliminarPorNumeroMiembro(114);
		assertThrows(ManagerException.class,()->miembroService.buscarPorNumeroMiembro(114));
		assertTrue(miembroService.obtenerListaMiembros().size() < cantidadInicial);
	}
	/**
	 * Prueba la funcionalidad de editar un miembro.
	 * Se verifica que un miembro se puede editar correctamente y que los cambios se reflejan en la base de datos.
	 */
	@Test
	@DisplayName("editar miembro")
	void editarMiembroTest(){
		int cantidad = miembroService.obtenerListaMiembros().size();
		// existe un miembro con num Miembro de 112 en la bd
		assertNotNull(miembroService.buscarPorNumeroMiembro(112));
		DocenteDto docente = new DocenteDto();
		docente.setId(miembroService.buscarPorNumeroMiembro(112).getId());
		docente.setCorreoElectronico(miembroService.buscarPorNumeroMiembro(112).getCorreoElectronico());
		docente.setNumeroMiembro(3);
		miembroService.editarMiembro(docente);
		assertEquals(cantidad,miembroService.obtenerListaMiembros().size());
		MiembroDto encontrado1 =miembroService.buscarPorNumeroMiembro(3);
		assertNotNull(encontrado1);
		assertThrows(ManagerException.class,()->miembroService.buscarPorNumeroMiembro(112));
		miembroService.eliminarPorNumeroMiembro(encontrado1.getNumeroMiembro());
	    assertNotEquals(miembroService.obtenerListaMiembros(),cantidad);	
	}
	/**
	 * metodo test que prueba la busqueda a los miembros
	 * con sus distintos metodos. usando miembros que ya
	 * existen en la base de datos
	 */
	@Test
	@DisplayName("buscar Miembro")
	void buscarMiembroTest(){
		//existe un miembro con el id 2
		assertEquals(miembroService.buscarPorId(2L).getNumeroMiembro(),miembroService.buscarPorNumeroMiembro(101).getNumeroMiembro());
		//existe un miembro con el numero de miembro 102 en la bd
		assertNotNull(miembroService.buscarPorNumeroMiembro(102));
		//existe un miembro con el id 1 en la bd
		assertNotNull(miembroService.buscarPorId(1L));
	}
	/**
	 * metodo test que controla que la validacion de agregar
	 * Miembro que es respecto a su correo Electronico 
	 * funcione correctamente
	 */
	@Test
	@DisplayName("validacion de AgregarMiembro")
	void validacionAgregarMiembroTest(){
		AlumnoDto alumno = new AlumnoDto();
		alumno.setCorreoElectronico("correo@gmail.com");
		alumno.setNumeroMiembro(4);
		miembroService.guardarMiembro(alumno);
		DocenteDto docente = new DocenteDto();
		docente.setCorreoElectronico("correo@gmail.com");
		assertThrows(ManagerException.class,()-> miembroService.guardarMiembro(docente));
	}
}
