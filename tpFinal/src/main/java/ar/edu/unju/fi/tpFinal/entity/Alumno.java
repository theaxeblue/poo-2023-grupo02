package ar.edu.unju.fi.tpFinal.entity;


import jakarta.persistence.Column;
import jakarta.persistence.DiscriminatorValue;
import jakarta.persistence.Entity;


@Entity
@DiscriminatorValue(value="alumno")
public class Alumno extends Miembro{

	@Column(name="Libreta_Universitaria")
	private String libretaUniversitaria;

	public String getLibretaUniversitaria() {
		return libretaUniversitaria;
	}

	public void setLibretaUniversitaria(String libretaUniversitaria) {
		this.libretaUniversitaria = libretaUniversitaria;
	}

	@Override
	public String toString() {
		return super.toString() + "Alumno [libretaUniversitaria=" + libretaUniversitaria + "]";
	}
	
}
