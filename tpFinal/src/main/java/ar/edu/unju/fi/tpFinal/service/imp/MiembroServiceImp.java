package ar.edu.unju.fi.tpFinal.service.imp;

import java.util.ArrayList;
import java.util.Optional;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ar.edu.unju.fi.tpFinal.dto.AlumnoDto;
import ar.edu.unju.fi.tpFinal.dto.DocenteDto;
import ar.edu.unju.fi.tpFinal.dto.MiembroDto;
import ar.edu.unju.fi.tpFinal.entity.Alumno;
import ar.edu.unju.fi.tpFinal.entity.Docente;
import ar.edu.unju.fi.tpFinal.entity.Miembro;
import ar.edu.unju.fi.tpFinal.exceptions.ManagerException;
import ar.edu.unju.fi.tpFinal.repository.MiembroRepository;
import ar.edu.unju.fi.tpFinal.service.MiembroService;

@Service
public class MiembroServiceImp implements MiembroService{
	public static Logger logger = Logger.getLogger(MiembroServiceImp.class);

	@Autowired
	private MiembroRepository miembroRepository;
	
	/**
	 * Guarda un miembro en la base de datos.
	 * @param miembro El miembro a ser guardado.
	 */
	@Override
	public MiembroDto guardarMiembro(MiembroDto miembroDto) throws ManagerException{
		// TODO Auto-generated method stub
		if(miembroRepository.findByCorreoElectronico(miembroDto.getCorreoElectronico())!=null){
			 logger.error("EL correo electronico ingresado ya está registrado.");
		    throw new ManagerException("no puede existir dos miembros con el mismo correo Electronico");
		}
		miembroDto.setDiasSancion(0);
		Miembro miembro = convertirMiembro(miembroDto);
		 logger.info("Se registra un nuevo miembro en la base de datos.");
		miembroRepository.save(miembro);
		return miembroDto;
	}
	/**
	 * Edita un miembro existente en la base de datos.
	 *
	 * @param miembro El miembro a ser editado.
	 */
	@Override
	public MiembroDto editarMiembro(MiembroDto miembroDto) throws ManagerException{
		// TODO Auto-generated method stub
		if(miembroDto.getId() == null){
			logger.error("se intento editar un miembro con un id nulo");
			throw new ManagerException("se debio ingresar un id de miembro a modificar");
		}
		if(miembroRepository.findById(miembroDto.getId()).orElse(null)== null){
			logger.error("se intento editar un miembro con un id invalido");
			throw new ManagerException("no se encontro un miembro con ese id para modificar");
		}
		Miembro miembro = convertirMiembro(miembroDto); 
		 logger.info("Se registra un nuevo miembro en la base de datos.");
		miembroRepository.save(miembro);
		miembroDto = convertirMiembroDto(miembroRepository.findById(miembro.getId()).get());
		return miembroDto;
	}
	/**
	 * Elimina un miembro de la base de datos.
	 *
	 * @param miembro El miembro a ser eliminado.
	 */
	@Override
	public MiembroDto eliminarMiembroPorId(Long id) throws ManagerException{
		// TODO Auto-generated method stub
		 logger.info("Se elimina miembro en la base de datos.");
		Miembro miembro = miembroRepository.findById(id).get();
		MiembroDto miembroDto = convertirMiembroDto(miembro);
		miembroRepository.deleteById(id);
		return miembroDto;
		
	}
	/**
	 * Obtiene una lista de todos los miembros en la base de datos.
	 *
	 * @return Una lista de miembros.
	 */
	@Override
	public ArrayList<MiembroDto> obtenerListaMiembros() {
		// TODO Auto-generated method stub
		ArrayList<Miembro> lista = miembroRepository.findAll();
		ArrayList<MiembroDto> listaDto = new ArrayList<>();
		 logger.info("Se obtiene la lista de miembros.");
		for(Miembro miembro : lista) {
			MiembroDto nuevo = convertirMiembroDto(miembro);
			listaDto.add(nuevo);
		}
		return listaDto;
	}
	/**
	 * Busca un miembro por id en la base de datos
	 *
	 * @param el id a buscar
	 * @return El miembro con el id coincidente o NUll si no lo encuentra
	 */
	@Override
	public MiembroDto buscarPorId(Long id) {
		// TODO Auto-generated method stub
		logger.info("Se busca miembro por Id");
		    Optional<Miembro> miembroTipo = miembroRepository.findById(id);
		    if (miembroTipo.isPresent()) {
		        Miembro miembro = miembroTipo.get();
		        MiembroDto miembroDto = convertirMiembroDto(miembro);
		        return miembroDto;
		    } else {
		        // Manejo cuando no se encuentra ningún Miembro con el ID dado
		        return null; // o lanzar una excepción, según tu lógica
		    }
	}
	/**
	 * Busca un miembro por el numero de miembro
	 *
	 * @param el numero de miembro a buscar
	 * @return El miembro con el numero de miembro coincidente o NUll si no lo encuentra
	 */
    @Override
	public MiembroDto buscarPorNumeroMiembro(Integer numeroMiembro) throws ManagerException{
    	if(miembroRepository.findByNumeroMiembro(numeroMiembro)==null){
 			logger.error("se intento buscar un Miembro con un numero de miembro invalido");
    		throw new ManagerException("no se encontro un miembro con ese id");
 		} 
    	 logger.info("Se busca miembro por su numero de miembro");
    	MiembroDto miembro = convertirMiembroDto(miembroRepository.findByNumeroMiembro(numeroMiembro));
	    return miembro;
	}
    /**
	 * elimina un miembro de la base de datos con el numero
	 * de miembro coincidente
	 *
	 * @param el codigo a buscar para eliminar al miembro
	 */
     @Override
     public MiembroDto eliminarPorNumeroMiembro(Integer numeroMiembro) throws ManagerException{
    	if(miembroRepository.findByNumeroMiembro(numeroMiembro)==null){
    		logger.error("se intento eliminar un miembro con un numero de miembro invalido");
 			throw new ManagerException("no se encontro un miembro con ese id para eliminar");
 		} 
       logger.info("Se miembro de la base de datos");
       Miembro miembro = miembroRepository.findByNumeroMiembro(numeroMiembro);
	   MiembroDto miembroDto = convertirMiembroDto(miembro);
	   miembroRepository.delete(miembroRepository.findByNumeroMiembro(numeroMiembro));
	   return miembroDto;
	}
     /**
 	 * convierte miembro a MiembroDto sea de las clases hijas
 	 * Alumno o Docente
 	 * @param el miembro a convertir
 	 * @return un objeto con los atributos de miembro o null
 	 */
	public MiembroDto convertirMiembroDto(Miembro miembro) {
		 logger.debug("Miembro conviertiendose en MiembroDTO");
		if(miembro instanceof Alumno){
		Alumno alumno2= (Alumno)miembro;
		AlumnoDto alumno = new AlumnoDto();
		alumno.setCorreoElectronico(alumno2.getCorreoElectronico());
		alumno.setNombre(alumno2.getNombre());
		alumno.setId(alumno2.getId());
		alumno.setNumeroMiembro(alumno2.getNumeroMiembro());
		alumno.setTelefono(alumno2.getTelefono());
		alumno.setLibretaUniversitaria(alumno2.getLibretaUniversitaria());
		alumno.setDiasSancion(alumno2.getDiasSancion());
		return alumno;
		}else{
				Docente docente2= (Docente)miembro;
				DocenteDto docente = new DocenteDto();
				docente.setCorreoElectronico(docente2.getCorreoElectronico());
				docente.setNombre(docente2.getNombre());
				docente.setId(docente2.getId());
				docente.setNumeroMiembro(docente2.getNumeroMiembro());
				docente.setTelefono(docente2.getTelefono());
				docente.setLegajo(docente2.getLegajo());
				docente.setDiasSancion(docente.getDiasSancion());
				return docente;
		}
     }
	/**
 	 * convierte miembroDto a Miembro sea de las clases hijas
 	 * Alumno o Docente
 	 * @param el miembroDto a convertir
 	 * @return un objeto con los atributos de miembro O null
 	 */
	public Miembro convertirMiembro(MiembroDto miembroDto) {
		 logger.debug("MiembroDTO conviertiendose en Miembro");
		if(miembroDto instanceof AlumnoDto) {
			if(miembroDto.getId() == null) {
			AlumnoDto alumno2= (AlumnoDto)miembroDto;
			Alumno alumno = new Alumno();
			alumno.setCorreoElectronico(alumno2.getCorreoElectronico());
			alumno.setNombre(alumno2.getNombre());
			alumno.setId(alumno2.getId());
			alumno.setNumeroMiembro(alumno2.getNumeroMiembro());
			alumno.setTelefono(alumno2.getTelefono());
			alumno.setLibretaUniversitaria(alumno2.getLibretaUniversitaria());
			alumno.setPrestamos(null);
			alumno.setDiasSancion(alumno2.getDiasSancion());
			return alumno;
			}else {
				Alumno alumno2= (Alumno)miembroRepository.findById(miembroDto.getId()).get();
				alumno2.setCorreoElectronico(miembroDto.getCorreoElectronico());
				alumno2.setNombre(miembroDto.getNombre());
				alumno2.setId(miembroDto.getId());
				alumno2.setNumeroMiembro(miembroDto.getNumeroMiembro());
				alumno2.setTelefono(miembroDto.getTelefono());
				alumno2.setDiasSancion(miembroDto.getDiasSancion());
				return alumno2;
			}
		}else {
				if(miembroDto.getId() == null) {
				DocenteDto docente2= (DocenteDto)miembroDto;
				Docente docente = new Docente();
				docente.setCorreoElectronico(docente2.getCorreoElectronico());
				docente.setNombre(docente2.getNombre());
				docente.setId(docente2.getId());
				docente.setNumeroMiembro(docente2.getNumeroMiembro());
				docente.setTelefono(docente2.getTelefono());
				docente.setLegajo(docente2.getLegajo());
				docente.setPrestamos(null);
				docente.setDiasSancion(docente2.getDiasSancion());
				return docente;
				}else {
					Docente docente2= (Docente)miembroRepository.findById(miembroDto.getId()).get();
					docente2.setCorreoElectronico(miembroDto.getCorreoElectronico());
					docente2.setNombre(miembroDto.getNombre());
					docente2.setId(miembroDto.getId());
					docente2.setNumeroMiembro(miembroDto.getNumeroMiembro());
					docente2.setTelefono(miembroDto.getTelefono());
					docente2.setDiasSancion(miembroDto.getDiasSancion());
					return docente2;
				}
		}
	}
}