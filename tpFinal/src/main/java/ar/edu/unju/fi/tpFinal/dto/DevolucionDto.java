package ar.edu.unju.fi.tpFinal.dto;

import java.io.Serializable;
import java.sql.Date;

public class DevolucionDto implements Serializable{
	private static final long serialVersionUID = 1L;
	private Long id;
	private Long prestamo;
	private Date devolucionEfectuada;
	private String nombreMiembro;
	private String libroTitulo;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getPrestamo() {
		return prestamo;
	}
	public void setPrestamo(Long id) {
		this.prestamo = id;
	}
	public Date getDevolucionEfectuada() {
		return devolucionEfectuada;
	}
	public void setDevolucionEfectuada(Date devolucionEfectuada) {
		this.devolucionEfectuada = devolucionEfectuada;
	}
	public String getNombreMiembro() {
		return nombreMiembro;
	}
	public void setNombreMiembro(String nombreMiembro) {
		this.nombreMiembro = nombreMiembro;
	}
	public String getLibroTitulo() {
		return libroTitulo;
	}
	public void setLibroTitulo(String libroTitulo) {
		this.libroTitulo = libroTitulo;
	}
}
