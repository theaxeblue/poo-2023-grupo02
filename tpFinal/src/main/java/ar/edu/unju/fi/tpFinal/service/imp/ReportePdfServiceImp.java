package ar.edu.unju.fi.tpFinal.service.imp;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

//import com.itextpdf.text.Document;
//import com.itextpdf.text.DocumentException;
//import com.itextpdf.text.Paragraph;
//import com.itextpdf.text.pdf.PdfDocument;
//import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.kernel.pdf.PdfWriter;
import com.itextpdf.layout.Document;
import com.itextpdf.layout.element.Cell;
import com.itextpdf.layout.element.Image;
import com.itextpdf.layout.element.Paragraph;
import com.itextpdf.layout.element.Table;
import com.itextpdf.layout.element.Text;
import com.itextpdf.layout.properties.TextAlignment;
import com.itextpdf.layout.properties.UnitValue;
import com.itextpdf.layout.properties.VerticalAlignment;
import com.itextpdf.io.image.ImageDataFactory;
import com.itextpdf.kernel.geom.PageSize;
import com.itextpdf.kernel.pdf.PdfDocument;
import com.itextpdf.kernel.colors.*;


import ar.edu.unju.fi.tpFinal.dto.PrestamoDto;
import ar.edu.unju.fi.tpFinal.exceptions.ManagerException;
import ar.edu.unju.fi.tpFinal.repository.DevolucionRepository;
import ar.edu.unju.fi.tpFinal.service.DevolucionService;
import ar.edu.unju.fi.tpFinal.service.PrestamoService;
import ar.edu.unju.fi.tpFinal.service.ReporteService;
import jakarta.servlet.ServletOutputStream;
import jakarta.servlet.http.HttpServletResponse;

@Service("pdf")
public class ReportePdfServiceImp implements ReporteService{

	public static Logger logger = Logger.getLogger(ReporteExcelServiceImp.class);
	
	@Autowired
	PrestamoService prestamoService;
	@Autowired
	DevolucionService devolucionService;
	@Autowired
	DevolucionRepository devolucionRepository;
	
	
	/**
     * Genera un resumen de préstamos en formato PDF.
     *
     * @param resumenPrestamo   HttpServletResponse para escribir la respuesta en el cliente.
     * @param fechaInicio       Fecha de inicio del periodo.
     * @param fechaFin          Fecha de fin del periodo.
     */
	@Override
	public void generarResumen(HttpServletResponse resumenPrestamo,Date fechaInicio, Date fechaFin) throws FileNotFoundException{
		// TODO Auto-generated method stub
		if(fechaInicio.getTime() > fechaFin.getTime()) {
			logger.error("la fecha inicio: "+fechaInicio+ " es mayor a la fecha final: "+fechaFin);
			throw new ManagerException("la fecha inicio no puede ser mayor a la fecha final");
		}
		 ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
	      PdfWriter writer = new PdfWriter(byteArrayOutputStream);
	      PdfDocument pdf = new PdfDocument(writer);
	      Document document = new Document(pdf);
	      PageSize pageSize = PageSize.TABLOID;
	      pdf.setDefaultPageSize(pageSize);
	      List<String> campos = obtenerListaCampos();
	      List<PrestamoDto> listaPrestamos = obtenerPrestamosFiltrados(fechaInicio, fechaFin);
	      Table table = (Table) crearTabla(listaPrestamos,campos);
	      document.add(table);
	      //Cerrar el documento
	      document.close();
	      try {
	      ServletOutputStream ops = resumenPrestamo.getOutputStream();
	      // Escribir el contenido en la respuesta
	      byteArrayOutputStream.writeTo(ops);
	      ops.flush();
	      logger.info("se escribio y cerro el documento correctamente para generar el resumen pdf");
	      }catch( IOException e) {
	    	  logger.error("ocurrio un error para la respuesta http: "+e.getMessage());
	    	  throw new ManagerException("Error al escribir en la respuesta HTTP"+ e.getMessage());
	      }
	}
	
	 /**
     * Obtiene la lista de campos para el resumen de préstamos.
     *
     * @return Lista de nombres de campos.
     */
	public List<String> obtenerListaCampos(){
		List<String> campos = new ArrayList<>();
		campos.add("id");
		campos.add("fechaYhoraPrestamo");
		campos.add("fechaYhoraDevolucion");
		campos.add("nombreMiembro");
		campos.add("TituloLibro");
		campos.add("estado");
		campos.add("DevolucionEfectuada");
		logger.info("se genera una lista de campos con el tamaño: "+campos.size());
		return campos;
	}
	/**
     * Crea una tabla para el resumen de préstamos en formato PDF.
     *
     * @param listaFiltrada Lista de préstamos filtrados.
     * @param campos        Lista de nombres de campos.
     * @return Objeto Table que representa la tabla.
     */
	public  Object crearTabla(List<PrestamoDto> listaFiltrada,List<String> campos){
		Table table = new Table(campos.size());
	      table.setWidth(UnitValue.createPercentValue(100));
	      DateTimeFormatter formatoFecha = DateTimeFormatter.ofPattern("yyyy/MM MMMM/dd EEEE");
	      float[] columnWidths = {1, 1, 1, 1, 1, 1, 1}; // Reducir los valores según sea necesario
	   // Ruta relativa al directorio resources
	      String imagePath = "logoBiblio.png";
	      // Carga de la imagen desde el recurso
	      Image image = new Image(ImageDataFactory.create(getClass().getResource("/" + imagePath)))
	              .scaleToFit(100, 100); // Ajusta el tamaño a 100x100 píxeles
	      // Crear un párrafo para el título y la imagen
	      Paragraph titleAndImage = new Paragraph()
	              .add(new Text("RESUMEN DE PRESTAMO").setFontSize(20)) // Ajusta el tamaño de la fuente según tus necesidades
	              .add("\n") // Agrega un salto de línea entre el título y la imagen
	              .add(image);
	      // Crea una celda del encabezado y agrega la imagen
	      table.addHeaderCell(new Cell(1, campos.size()).add(titleAndImage).setTextAlignment(TextAlignment.CENTER).
	    		  setVerticalAlignment(VerticalAlignment.MIDDLE).setBackgroundColor(ColorConstants.LIGHT_GRAY));
	      // Añade celdas de encabezado
	      for (int i = 0; i < campos.size(); i++) {
	          table.addHeaderCell(new Cell().add(new Paragraph(campos.get(i))).setWidth(columnWidths[i]).setBackgroundColor(ColorConstants.LIGHT_GRAY));
	      }
	      // Añade una fila por cada objeto
	      for (PrestamoDto prestamoDto : listaFiltrada) {
	          List<Object> atributos = obtenerAtributos(prestamoDto);
	          for (int i = 0; i < atributos.size(); i++) {
	              // Añade celdas de datos
	        	  if(atributos.get(i) instanceof Date) {
	        		  Date sqlDate = (Date) atributos.get(i);
	                  java.util.Date utilDate = new java.util.Date(sqlDate.getTime());
	        		  String fechaFormateada = utilDate.toInstant().atZone(ZoneId.systemDefault()).toLocalDate().format(formatoFecha);
		              table.addCell(new Paragraph(fechaFormateada));
	        	  }else {
	        		  table.addCell(new Cell().add(new Paragraph(String.valueOf(atributos.get(i)))).setWidth(columnWidths[i]));  
	        	  }
	          }
	      }
          LocalDate utilDate = LocalDate.now();
		  String fechaFormateada = utilDate.format(formatoFecha);
	      Paragraph fechaResumen = new Paragraph()
	              .add("\nFecha del Resumen: " + fechaFormateada);
	      table.addFooterCell(new Cell(1, campos.size()).add(fechaResumen).setTextAlignment(TextAlignment.CENTER).
	    		  setVerticalAlignment(VerticalAlignment.MIDDLE).setBackgroundColor(ColorConstants.LIGHT_GRAY));
	      logger.info("se obtiene el libro con la tabla de resumen pdf");
		return table;
	}
	
	
	/**
     * Obtiene la lista de préstamos filtrados por un rango de fechas.
     *
     * @param fechaInicio Fecha de inicio del periodo.
     * @param fechaFin    Fecha de fin del periodo.
     * @return Lista de préstamos filtrados.
     */
	@Override
	public List<PrestamoDto> obtenerPrestamosFiltrados(Date fechaInicio, Date fechaFin) {
		// TODO Auto-generated method stub
				List<PrestamoDto> listaPrestamo = prestamoService.obtenerListaPrestamos();
				List<PrestamoDto> listaFiltrada = new ArrayList<PrestamoDto>();
				for(PrestamoDto presta : listaPrestamo){
					if((presta.getFechaYhoraPrestamo().getTime() >= fechaInicio.getTime())&&
							(presta.getFechaYhoraPrestamo().getTime() <= fechaFin.getTime())){
						listaFiltrada.add(presta);	
					}
				}
				logger.info("se obtiene una lista de prestamos del tamaño: "+listaFiltrada.size());
				return listaFiltrada;
	}
	/**
     * Obtiene los atributos de un objeto PrestamoDto.
     *
     * @param prestamoDto Objeto PrestamoDto.
     * @return Lista de atributos.
     */
	@Override
	public List<Object> obtenerAtributos(PrestamoDto prestamoDto) {
		List<Object> lista = new ArrayList<>();
		lista.add(prestamoDto.getId());
		lista.add(prestamoDto.getFechaYhoraPrestamo());
		lista.add(prestamoDto.getFechaYhoraDevolucion());
		lista.add(prestamoDto.getNombreMiembro());
		lista.add(prestamoDto.getTitulolibro());
		lista.add(prestamoDto.getEstado());
		if(prestamoDto.getDevolucion() != null) {
			lista.add(devolucionRepository.findById(prestamoDto.getDevolucion()).get().getDevolucionEfectuada());
		}else {
		   lista.add("No se Devolvio todavia");
		}
		logger.info("obtiene una lista de los atributos del prestamo con tamaño: "+lista.size());
		return lista;
	}
}
