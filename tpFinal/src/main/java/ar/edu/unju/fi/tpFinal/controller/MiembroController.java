package ar.edu.unju.fi.tpFinal.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ar.edu.unju.fi.tpFinal.dto.AlumnoDto;
import ar.edu.unju.fi.tpFinal.dto.DocenteDto;
import ar.edu.unju.fi.tpFinal.dto.MiembroDto;
import ar.edu.unju.fi.tpFinal.exceptions.ManagerException;
import ar.edu.unju.fi.tpFinal.service.MiembroService;


@RestController
@RequestMapping("/biblioteca/v1/miembro")
public class MiembroController {
	private final Logger logger = LoggerFactory.getLogger(MiembroController.class);
	@Autowired
	MiembroService miembroService;
	/**
     * Obtiene la lista de miembros.
     * @return ResponseEntity con la lista de miembros y mensajes asociados.
     */
	@GetMapping("/miembros")
	public ResponseEntity<?> obtenerMiembros(){
		Map<String,Object> response = new HashMap<String,Object>();
		List<MiembroDto> listaDto = new ArrayList<MiembroDto>();
		try {
			logger.info("iniciando la obtencion de la lista de miembros");
			listaDto = miembroService.obtenerListaMiembros();
			response.put("Miembros: ",listaDto );
			response.put("Mensaje", "objetos consultados correctamente");
		}catch(DataAccessException e){
			response.put("Mensaje", "Error al intentar consultar la lista de miembros");
			response.put("Error", e.getCause().getMessage());
			return new ResponseEntity<Map<String,Object>>(response,HttpStatus.INTERNAL_SERVER_ERROR);
		}
		logger.info("se retorna la respuesta sin problemas de la obtencion de"
				+ "la lista con: "+listaDto.size()+" miembros");
		return new ResponseEntity<Map<String,Object>>(response,HttpStatus.OK);
	 }
	
	/**
     * Registra un nuevo miembro del tipo alumno.
     * @param alumnoDto Objeto alumnoDto con la información del nuevo miembro.
     * @return ResponseEntity con el resultado de la operación y mensajes asociados.
     */
	@PostMapping("/registrarAlumno")
	   public ResponseEntity<?> registrarAlumno(@RequestBody AlumnoDto alumnoDto){
		Map<String,Object> response = new HashMap<String,Object>();
		try {
			logger.info("iniciando el registro de miembro con el numero de Miembro: "+ alumnoDto.getNumeroMiembro());
			response.put("Miembro: ",miembroService.guardarMiembro(alumnoDto));
			response.put("Mensaje", "objeto registrado correctamente");
		}catch(ManagerException e){
			response.put("Mensaje", "Error al intentar registrar El objeto");
			response.put("Error", e.getMessage());
			return new ResponseEntity<Map<String,Object>>(response,HttpStatus.INTERNAL_SERVER_ERROR);
		}
		logger.info("se retorna la respuesta sin problemas de el registro del miembro con"
				+ "el numero de miembro: "+ alumnoDto.getNumeroMiembro());
		return new ResponseEntity<Map<String,Object>>(response,HttpStatus.CREATED);
	 }
	/**
     * Registra un nuevo miembro del tipo docente.
     * @param docenteDto Objeto docenteDto con la información del nuevo miembro.
     * @return ResponseEntity con el resultado de la operación y mensajes asociados.
     */
	@PostMapping("/registrarDocente")
	   public ResponseEntity<?> registrarDocente(@RequestBody DocenteDto docenteDto){
		Map<String,Object> response = new HashMap<String,Object>();
		try {
			logger.info("iniciando el registro de miembro con el numero de Miembro: "+ docenteDto.getNumeroMiembro());
			response.put("Miembro: ",miembroService.guardarMiembro(docenteDto));
			response.put("Mensaje", "objeto registrado correctamente");
		}catch(ManagerException e){
			response.put("Mensaje", "Error al intentar registrar El objeto");
			response.put("Error", e.getMessage());
			return new ResponseEntity<Map<String,Object>>(response,HttpStatus.INTERNAL_SERVER_ERROR);
		}
		logger.info("se retorna la respuesta sin problemas de el registro del miembro con"
				+ "el numero de miembro: "+ docenteDto.getNumeroMiembro());
		return new ResponseEntity<Map<String,Object>>(response,HttpStatus.CREATED);
	 }
	/**
     * Modifica un miembro existente del tipo alumno.
     * @param alumnoDto Objeto AlumnoDto con la información del miembro a modificar.
     * @return ResponseEntity con el resultado de la operación y mensajes asociados.
     */
	@PutMapping("/modificarAlumno")
	public ResponseEntity<?> modificarAlumno(@RequestBody AlumnoDto alumnoDto){
		Map<String,Object> response = new HashMap<String,Object>();
		try {
			logger.info("iniciando la modificacion del miembro con el Id: "+alumnoDto.getId());
			response.put("Miembro: ",miembroService.editarMiembro(alumnoDto));
			response.put("Mensaje", "objeto modificado correctamente");
		}catch(ManagerException e){
			response.put("Mensaje", "Error al intentar modificar El objeto");
			response.put("Error", e.getMessage());
			return new ResponseEntity<Map<String,Object>>(response,HttpStatus.INTERNAL_SERVER_ERROR);
		}
		logger.info("se retorna la respuesta sin problemas de la modificacion del miembro"
				+ "con el id:"+ alumnoDto.getId());
		return new ResponseEntity<Map<String,Object>>(response,HttpStatus.OK);
	 }
	/**
     * Modifica un miembro existente del tipo docente.
     * @param docenteDto Objeto DocenteDto con la información del miembro a modificar.
     * @return ResponseEntity con el resultado de la operación y mensajes asociados.
     */
	@PutMapping("/modificarDocente")
	public ResponseEntity<?> modificarDocente(@RequestBody DocenteDto docenteDto){
		Map<String,Object> response = new HashMap<String,Object>();
		try {
			logger.info("iniciando la modificacion del miembro con el Id: "+docenteDto.getId());
			response.put("Miembro: ",miembroService.editarMiembro(docenteDto));
			response.put("Mensaje", "objeto modificado correctamente");
		}catch(ManagerException e){
			response.put("Mensaje", "Error al intentar modificar El objeto");
			response.put("Error", e.getMessage());
			return new ResponseEntity<Map<String,Object>>(response,HttpStatus.INTERNAL_SERVER_ERROR);
		}
		logger.info("se retorna la respuesta sin problemas de la modificacion del miembro"
				+ "con el id:"+ docenteDto.getId());
		return new ResponseEntity<Map<String,Object>>(response,HttpStatus.OK);
	 }
	/**
     * Elimina un miembro por su número de miembro.
     * @param numeroMiembro Número del miembro a eliminar.
     * @return ResponseEntity con el resultado de la operación y mensajes asociados.
     */
	@DeleteMapping("/eliminarMiembro/{numeroMiembro}")
	public ResponseEntity<?> elimarMiembro(@PathVariable Integer numeroMiembro){
		Map<String,Object> response = new HashMap<String,Object>();
		try {
			logger.info("iniciando la eliminacion del Miembro con el numero De miembro: "+numeroMiembro);
			response.put("Miembro: ",miembroService.eliminarPorNumeroMiembro(numeroMiembro));
			response.put("Mensaje", "objeto eliminado correctamente");
		}catch(ManagerException e){
			response.put("Mensaje", "Error al intentar eliminar El objeto");
			response.put("Error", e.getMessage());
			return new ResponseEntity<Map<String,Object>>(response,HttpStatus.INTERNAL_SERVER_ERROR);
		}
		logger.info("se retorna la respuesta sin problemas de la eliminacion del miembro con el"
				+ "numero de miembro: "+numeroMiembro);
		return new ResponseEntity<Map<String,Object>>(response,HttpStatus.OK);
	 }
	/**
     * Busca un miembro por su número de miembro.
     * @param numeroMiembro Número del miembro a buscar.
     * @return ResponseEntity con el resultado de la operación y mensajes asociados.
     */
	@GetMapping("/buscarMiembro/{numeroMiembro}")
	public ResponseEntity<?> buscarMiembroPorNumeroMiembro(@PathVariable Integer numeroMiembro){
		Map<String,Object> response = new HashMap<String,Object>();
		try {
			logger.info("iniciando la obtencion del miembro con el numero de miembro: "+ numeroMiembro);
			response.put("Miembro: ",miembroService.buscarPorNumeroMiembro(numeroMiembro));
			response.put("Mensaje", "objeto encontrado correctamente");
		}catch(ManagerException e){
			response.put("Mensaje", "Error al intentar buscar El objeto");
			response.put("Error", e.getMessage());
			return new ResponseEntity<Map<String,Object>>(response,HttpStatus.INTERNAL_SERVER_ERROR);
		}
		logger.info("se retorna la respuesta sin problemas de la obtencion del miembro"
				+ " con el Numero de miembro: "+numeroMiembro);
		return new ResponseEntity<Map<String,Object>>(response,HttpStatus.OK);
	}
}
