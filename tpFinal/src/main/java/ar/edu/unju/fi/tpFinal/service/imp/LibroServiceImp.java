package ar.edu.unju.fi.tpFinal.service.imp;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ar.edu.unju.fi.tpFinal.dto.LibroDto;
import ar.edu.unju.fi.tpFinal.entity.Libro;
import ar.edu.unju.fi.tpFinal.exceptions.ManagerException;
import ar.edu.unju.fi.tpFinal.repository.LibroRepository;
import ar.edu.unju.fi.tpFinal.service.LibroService;

@Service
public class LibroServiceImp implements LibroService {
	public static Logger logger = Logger.getLogger(LibroServiceImp.class);

	@Autowired
	private LibroRepository libroRepository;
	
	/**
	 * Guarda un libro en la base de datos.
	 *
	 * @param libro El libro a ser guardado.
	 */
	@Override
	public LibroDto guardarLibro(LibroDto libroDto) throws ManagerException{
		// TODO Auto-generated method stub
		if(libroRepository.findByIsbn(libroDto.getIsbn())!=null) {
			logger.error("EL ISBN ya está registrado con otro libro.");
			throw new ManagerException("No se puede tener libros con el mismo isbn");
		}
		if(libroRepository.findByNumeroInventario(libroDto.getNumeroInventario())!=null){
			logger.error("Numero de inventario ya está usado.");
			throw new ManagerException("no se puede tener libros con el mismo numero de inventario");
		}
		libroDto.setEstado("Disponible");
		Libro libro = convertirLibro(libroDto);
		logger.info("Se registra un nuevo libro en la base de datos.");
		libroRepository.save(libro);
		return libroDto;
	}
	/**
	 * Modifica un libro existente en la base de datos.
	 *
	 * @param libro El libro a ser modificado.
	 */
	@Override
	public LibroDto modificarLibro(LibroDto libroDto) throws ManagerException{
		// TODO Auto-generated method stub
		if(libroDto.getId() == null){
			logger.error("se intento modificar un libro con un id nulo");
			throw new ManagerException("se debio de ingresar un id de libro");
		}
		if(libroRepository.findById(libroDto.getId()).orElse(null)== null){
			logger.error("se intento modificar un libro con un id invalido");
			throw new ManagerException("no se encontro un libro con ese id");
		}
		Libro libro = convertirLibro(libroDto);
		logger.info("Se modica registro de un libro en la base de datos.");
		libroRepository.save(libro);
		libroDto = convertirLibroDto(libroRepository.findById(libro.getId()).get());
		return libroDto;
	}
	/**
	 * Elimina un libro de la base de datos.
	 *
	 * @param libro El libro a ser eliminado.
	 */
	@Override
	public LibroDto eliminarLibroPorIsbn(Integer isbn) throws ManagerException{
		// TODO Auto-generated method stub
		if(libroRepository.findByIsbn(isbn) == null){
			logger.error("se intento eliminar un libro con un isbn invalido");
			throw new ManagerException("no se encontro el libro con ese isbn");
		}
		logger.info("Se elimina un libro en la base de datos.");
		LibroDto libroDto = convertirLibroDto(libroRepository.findByIsbn(isbn));
		libroRepository.delete(libroRepository.findByIsbn(isbn));
		return libroDto;
	}
	/**
	 * Busca un libro por el id en la base de datos
	 *
	 * @param el id del libro a buscar
	 * @return El libro convertido a dto
	 */
	@Override
	    public LibroDto buscarPorId(Long id){
		// TODO Auto-generated method stub
		logger.info("Se busca libro por id");
		Libro libro =libroRepository.findById(id).orElse(null);
		LibroDto libroDto = convertirLibroDto(libro);
		return libroDto;
	}
	/**
	 * Busca un libro por su ISBN en la base de datos.
	 *
	 * @param isbn El ISBN del libro a buscar.
	 * @return El libro encontrado o null si no se encuentra.
	 */
	@Override
	public LibroDto buscarPorIsbn(Integer isbn) throws ManagerException{
		// TODO Auto-generated method stub
		if(libroRepository.findByIsbn(isbn) == null){
			logger.error("se intento buscar un libro con un isbn invalido");
			throw new ManagerException("no se encontro el libro con ese isbn");
		}
		logger.info("Se busca libro por ISBN");
		Libro libro = libroRepository.findByIsbn(isbn);
		LibroDto libroDto = convertirLibroDto(libro);
		return libroDto;
	}
	/**
	 * Busca un libro por su autor en la base de datos.
	 *
	 * @param autor El autor del libro a buscar.
	 * @return El libro encontrado o null si no se encuentra.
	 */
	@Override
	public ArrayList<LibroDto> buscarPorAutor(String autor){
		// TODO Auto-generated method stub
		ArrayList<LibroDto> listaLibrosDto = new ArrayList<>();
		logger.info("Se busca libro por Autor");
		List<Libro> listaLibros = libroRepository.findAllByAutor(autor);
		for(Libro lib : listaLibros){
			LibroDto libroDto = convertirLibroDto(lib);
			listaLibrosDto.add(libroDto);
		}
		return listaLibrosDto;
	}
	/**
	 * Obtiene una lista de todos los libros en la base de datos.
	 *
	 * @return Una lista de libros.
	 */
	@Override
	public ArrayList<LibroDto> obtenerListaLibros() {
		// TODO Auto-generated method stub
		ArrayList<Libro> lista = libroRepository.findAll();
		ArrayList<LibroDto> listaDto = new ArrayList<>();
		 logger.info("Se obtiene la lista de libros.");
		for(Libro lib : lista) {
			LibroDto libroDto = convertirLibroDto(lib);
			listaDto.add(libroDto);
		}
		return listaDto;
	}

	/**
	 * convierte un libro a LibroDto
	 * @param el libro a convertir
	 * @return un objeto con los atributos de libro
	 */
	public LibroDto convertirLibroDto(Libro libro) {
		 logger.debug("Libro conviertiendose en DTO");
		LibroDto libroD = new LibroDto();
		libroD.setId(libro.getId());
		libroD.setAutor(libro.getAutor());
		libroD.setEstado(libro.getEstado());
		libroD.setIsbn(libro.getIsbn());
		libroD.setNumeroInventario(libro.getNumeroInventario());
		libroD.setTitulo(libro.getTitulo());
	    return libroD;
	}
	/**
	 * convierte un libroDto a libro
	 * @param libroDto a convertir
	 * @return un objeto con los atributos de libroDto
	 */
	public Libro convertirLibro(LibroDto libroDto){
		 logger.debug("LibroDto conviertiendose en Libro");
		 if(libroDto.getId() == null) {
			Libro libro = new Libro();
			libro.setAutor(libroDto.getAutor());
			libro.setEstado(libroDto.getEstado());
			libro.setId(libroDto.getId());
			libro.setIsbn(libroDto.getIsbn());
			libro.setNumeroInventario(libroDto.getNumeroInventario());
			libro.setTitulo(libroDto.getTitulo());
			return libro;
		 }else {
			    Libro libro = libroRepository.findById(libroDto.getId()).get();
			    libro.setId(libroDto.getId());
				libro.setAutor(libroDto.getAutor());
				libro.setIsbn(libroDto.getIsbn());
				libro.setEstado(libroDto.getEstado());
				libro.setTitulo(libroDto.getTitulo());
				libro.setNumeroInventario(libroDto.getNumeroInventario());
			 return libro;
		 }
	}
}