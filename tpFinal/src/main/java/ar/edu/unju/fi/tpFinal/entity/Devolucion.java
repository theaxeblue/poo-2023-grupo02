package ar.edu.unju.fi.tpFinal.entity;

import java.sql.Date;

import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.OneToOne;
import jakarta.persistence.Table;

@Entity
@Table(name="Devolucion")
@Order(Ordered.LOWEST_PRECEDENCE)
public class Devolucion {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="devolucion_id")
    private Long id;
    @OneToOne
    @JoinColumn(name = "prestamo_id")
	private Prestamo prestamo;
    @Column(name="devolucion_efectuada")
	private Date devolucionEfectuada;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Prestamo getPrestamo() {
		return prestamo;
	}
	public void setPrestamo(Prestamo prestamo) {
		this.prestamo = prestamo;
	}
	public Date getDevolucionEfectuada() {
		return devolucionEfectuada;
	}
	public void setDevolucionEfectuada(Date devolucionEfectuada) {
		this.devolucionEfectuada = devolucionEfectuada;
	}
}
