package ar.edu.unju.fi.tpFinal.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ar.edu.unju.fi.tpFinal.dto.PrestamoDto;
import ar.edu.unju.fi.tpFinal.exceptions.ManagerException;
import ar.edu.unju.fi.tpFinal.service.LibroService;
import ar.edu.unju.fi.tpFinal.service.MiembroService;
import ar.edu.unju.fi.tpFinal.service.PrestamoService;
import jakarta.mail.MessagingException;

@RestController
@RequestMapping("/biblioteca/v1/prestamo")
public class PrestamoController{
	private final Logger logger = LoggerFactory.getLogger(PrestamoController.class);
	
	@Autowired
	PrestamoService prestamoService;
	@Autowired
	MiembroService miembroService;
	@Autowired
	LibroService libroService;
	 /**
     * Obtiene la lista de préstamos.
     * @return ResponseEntity con la lista de préstamos y mensajes asociados.
     */
	@GetMapping("/prestamos")
	public ResponseEntity<?> obtenerPrestamos(){
		Map<String, Object> response = new HashMap<String, Object>();
		List<PrestamoDto> listaDto = new ArrayList<PrestamoDto>();
		try {
			logger.info("iniciando obtencion de la lista de prestamos");
			listaDto = prestamoService.obtenerListaPrestamos();
			response.put("Prestamos", listaDto);
			response.put("Mensaje", "objetos consultados correctamente");
		} catch (ManagerException e) {
			response.put("Mensaje", "Error al intentar consultar la lista de prestamos");
			response.put("Error", e.getCause().getMessage());
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		logger.info("se retorna la respuesta sin problemas de la obtencion"
				+ "de la lista con: "+listaDto.size()+ " prestamos");
		return new ResponseEntity<Map<String, Object>>(response, HttpStatus.OK);
	}
	/**
     * Registra un nuevo préstamo.
     * @param prestamoDto Objeto PrestamoDto con la información del nuevo préstamo.
     * @return ResponseEntity con el resultado de la operación y mensajes asociados.
	 * @throws MessagingException 
     */
	@PostMapping("/registrarPrestamo/{idMiembro}/{idLibro}")
	public ResponseEntity<?> registrarPrestamo(@PathVariable Long idMiembro,@PathVariable Long idLibro) throws MessagingException{
		Map<String, Object> response = new HashMap<String, Object>();
		try {
			logger.info("iniciando el registro de prestamo con el id de Miembro: "+idMiembro
					+ "y el id de libro: "+idLibro);
			response.put("Prestamo",prestamoService.registrarPrestamo(idMiembro,idLibro));
			response.put("Mensaje", "Objeto Creado Correctamente");
			response.put("Mensaje2", "Correo Enviado Correctamente");
		}catch(ManagerException e) {
			response.put("Mensaje", "Error al intentar guardar el objeto");
			response.put("Error", e.getMessage());
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		logger.info("se retorna la respuesta sin problemas al registro del prestamo con el"
				+ "id Miembro: "+ idMiembro + " y el id libro: "+idLibro);
		return new ResponseEntity<Map<String, Object>>(response, HttpStatus.CREATED);
	}
	 /**
     * Modifica un préstamo existente.
     * @param prestamoDto Objeto PrestamoDto con la información del préstamo a modificar.
     * @return ResponseEntity con el resultado de la operación y mensajes asociados.
     */
	@PutMapping("/modificarPrestamo")
	public ResponseEntity<?> modificarPrestamo(@RequestBody PrestamoDto prestamoDto){
		Map<String, Object> response = new HashMap<String, Object>();
		try {
			logger.info("iniciando la modificacion del prestamo con el id: "+prestamoDto);
			response.put("Prestamo",prestamoService.modificarPrestamo(prestamoDto));
			response.put("Mensaje", "Objeto fue modificado correctamente");
		}catch(ManagerException e){
			response.put("Mensaje", "Erro al intentar modificar el objeto");
			response.put("Error", e.getMessage());
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		logger.info("se retorna la respuesta sin problemas la modificacion del"
				+ "prestamo con el id: "+ prestamoDto.getId());
		return new ResponseEntity<Map<String, Object>>(response, HttpStatus.OK);
	}
	/**
     * Elimina un préstamo por su ID.
     * @param id ID del préstamo a eliminar.
     * @return ResponseEntity con el resultado de la operación y mensajes asociados.
     */
	@DeleteMapping("/eliminarPrestamo/{id}")
	public ResponseEntity<?> eliminarPrestamo(@PathVariable Long id){
		Map<String,Object> response = new HashMap<String,Object>();
		try {
			logger.info("iniciando la eliminacion del prestamo con el id: "
				 + id);
			response.put("Prestamo: ",prestamoService.eliminarPorId(id));
			response.put("Mensaje", "objeto eliminado correctamente");
		}catch(ManagerException e){
			response.put("Mensaje", "Error al intentar eliminar el objeto");
			response.put("Error", e.getMessage());
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		logger.info("se retorna la respuesta sin problemas de la eliminacion del"
				+ "prestamo con el id: "+ id);
		return new ResponseEntity<Map<String,Object>>(response,HttpStatus.OK);
	 }
	/**
     * Busca un préstamo por su ID.
     * @param id ID del préstamo a buscar.
     * @return ResponseEntity con el resultado de la operación y mensajes asociados.
     */
	@GetMapping("/buscarPrestamo/{id}")
	public ResponseEntity<?> buscarPrestamo(@PathVariable Long id){
		Map<String,Object> response = new HashMap<String,Object>();
		try {
			logger.info("inicia la obtencion del prestamo con el id: "+id);
			response.put("Prestamo: ",prestamoService.buscarPrestamoPorId(id));
			response.put("Mensaje", "objeto encontrado correctamente");
		}catch(ManagerException e){
			response.put("Mensaje", "Error al intentar buscar el prestamo");
			response.put("Error", e.getMessage());
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);	
		}
		logger.info("se retorna la respuesta sin problemas de la obtencion"
				+ "del prestamo con el id: "+ id);
		return new ResponseEntity<Map<String,Object>>(response,HttpStatus.OK);
	 }
}
