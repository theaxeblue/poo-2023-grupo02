package ar.edu.unju.fi.tpFinal.service;

import java.io.FileNotFoundException;
import java.io.IOException;

import com.itextpdf.kernel.geom.PageSize;
import com.itextpdf.kernel.pdf.PdfDocument;
import com.itextpdf.layout.element.Image;
import com.itextpdf.layout.element.Table;

import jakarta.servlet.http.HttpServletResponse;

public interface ComprobanteService {

	 /**
     * Genera un comprobante de préstamo y lo devuelve como respuesta HTTP.
     *
     * @param resumenPrestamo Objeto para escribir la respuesta.
     * @param id              Identificador del préstamo para el cual se genera el comprobante.
     */
	public void generarComprobante(HttpServletResponse resumenPrestamo,Long id) throws FileNotFoundException, IOException;
	/**
     * Genera y devuelve una tabla con los datos del comprobante.
     *
     * @param pdf      Objeto para establecer el tamaño de página.
     * @param pageSize Tamaño de la página para el documento PDF.
     * @param id       Identificador del préstamo para el cual se genera la tabla.
     * @param image    Objeto que se incluirá en la tabla.
     * @return Objeto que representa la tabla generada.
     */
	public Table armarDatosDeComprobante(PdfDocument pdf,PageSize pageSize,Long id,Image image);
	/**
     * Genera y devuelve una tabla con las firmas del comprobante.
     *
     * @param image Objeto que se incluirá en la tabla de firmas.
     * @return Objeto que representa la tabla de firmas generada.
     */
	public Table armarFirmasDeComprobante(Image image);
}
