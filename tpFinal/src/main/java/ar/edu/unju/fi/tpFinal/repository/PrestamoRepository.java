package ar.edu.unju.fi.tpFinal.repository;

import java.util.ArrayList;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;


import ar.edu.unju.fi.tpFinal.entity.Prestamo;
@Repository
public interface PrestamoRepository extends CrudRepository<Prestamo,Long> {
	
	/**
	 * Obtiene una lista de todos los prestamos registrados en la biblioteca.
	 * @return Una lista de prestamos.
	 */
	public ArrayList<Prestamo> findAll ();
	/**
	 * busca el prestamo con un codigo para eliminarlo de la
	 * base de datos
	 * @param el codigo para buscar el prestamo a eliminar
	 */
	public void deleteById(Long id);
	

}
