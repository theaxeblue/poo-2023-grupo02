package ar.edu.unju.fi.tpFinal.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ar.edu.unju.fi.tpFinal.dto.LibroDto;
import ar.edu.unju.fi.tpFinal.exceptions.ManagerException;
import ar.edu.unju.fi.tpFinal.service.LibroService;
@RestController
@RequestMapping("/biblioteca/v1/libro")
public class LibroController {
	private final Logger logger = LoggerFactory.getLogger(LibroController.class);
	@Autowired
	LibroService libroService;
	/**
     * Obtiene la lista de libros.
     * @return ResponseEntity con la lista de libros y mensajes asociados.
     */
	@GetMapping("/libros")
	public ResponseEntity<?> obtenerLibros(){
		Map<String,Object> response = new HashMap<String,Object>();
		List<LibroDto> listaDto = new ArrayList<LibroDto>();
		try {
			logger.info("iniciando la obtencion de la lista de libros");
			listaDto = libroService.obtenerListaLibros();
			response.put("libros: ", listaDto);
			response.put("Mensaje", "objetos consultados correctamente");
		}catch(ManagerException e) {
			response.put("Mensaje: ", "Error al intentar consultar la lista de libros");
			response.put("Error: ", e.getCause().getMessage());
			return new ResponseEntity<Map<String,Object>>(response,HttpStatus.INTERNAL_SERVER_ERROR);
		}
		logger.info("se retorna la respuesta sin problemas de la obtencion"
				+ "de la lista con: "+ listaDto.size()+ " libros");
		return new ResponseEntity<Map<String,Object>>(response,HttpStatus.OK);
	 }
	/**
     * Registra un nuevo libro.
     * @param libroDto Objeto LibroDto con la información del nuevo libro.
     * @return ResponseEntity con el resultado de la operación y mensajes asociados.
     */
	@PostMapping("/registrarLibro")
	public ResponseEntity<?> registrarLibro(@RequestBody LibroDto libroDto){
		Map<String,Object> response = new HashMap<String,Object>();
		try {
			logger.info("iniciando el registro de un libro con el numero"
					+ "de inventario: "+ libroDto.getNumeroInventario());
			response.put("Libro", libroService.guardarLibro(libroDto));
			response.put("Mensaje", "Libro registrado correctamente");
		}catch(ManagerException e){
			response.put("Mensaje", "Error al intentar registrar El objeto");
			response.put("Error", e.getMessage());
			return new ResponseEntity<Map<String,Object>>(response,HttpStatus.INTERNAL_SERVER_ERROR);
		}
		logger.info("se retorna la respuesta sin problemas el registro"
				+ "del libro con el numero de inventario"+ libroDto.getNumeroInventario());
		return new ResponseEntity<Map<String,Object>>(response,HttpStatus.OK);
	 }
	/**
     * Modifica un libro existente.
     * @param libroDto Objeto LibroDto con la información del libro a modificar.
     * @return ResponseEntity con el resultado de la operación y mensajes asociados.
     */
	@PutMapping("/modificarLibro")
	public ResponseEntity<?> modificarLibro(@RequestBody LibroDto libroDto){
		Map<String,Object> response = new HashMap<String,Object>();
		try {
			logger.info("iniciando la modificacion del libro con el id: "+libroDto.getId());
			response.put("Libro", libroService.modificarLibro(libroDto));
			response.put("Mensaje", "Se modifico el objeto correctamente");
		}catch(ManagerException e){
			response.put("Mensaje", "Error al intentar modificar El objeto");
			response.put("Error", e.getMessage());
			return new ResponseEntity<Map<String,Object>>(response,HttpStatus.INTERNAL_SERVER_ERROR);
		}
		logger.info("se retorna la respuesta sin problemas la modificacion"
				+ "del libro  con el id: "+ libroDto.getId());
		return new ResponseEntity<Map<String,Object>>(response,HttpStatus.OK);
	}
	/**
     * Elimina un libro por su ISBN.
     * @param isbn ISBN del libro a eliminar.
     * @return ResponseEntity con el resultado de la operación y mensajes asociados.
     */
	@DeleteMapping("/eliminarLibro/{isbn}")
	public ResponseEntity<?> eliminarLibro(@PathVariable Integer isbn){
		Map<String,Object> response = new HashMap<String,Object>();
		try {
			logger.info("iniciando la eliminacion del libro con el isbn: "+ isbn);
			response.put("Libro", libroService.eliminarLibroPorIsbn(isbn));
			response.put("Mensaje", "Se elimino el objeto correctamente");
		}catch(ManagerException e){
			response.put("Mensaje", "Error al intentar eliminar El objeto");
			response.put("Error", e.getMessage());
			return new ResponseEntity<Map<String,Object>>(response,HttpStatus.INTERNAL_SERVER_ERROR);
		}
		logger.info("se retorna la respuesta sin problemas de la eliminacion del libro"
				+ "con el isbn: "+isbn);
		return new ResponseEntity<Map<String,Object>>(response,HttpStatus.OK);
	}
	/**
     * Busca un libro por su ISBN.
     * @param isbn ISBN del libro a buscar.
     * @return ResponseEntity con el resultado de la operación y mensajes asociados.
     */
	@GetMapping("/buscarLibroPorIsbn/{isbn}")
	public ResponseEntity<?> bucarLibroPorIsbn(@PathVariable Integer isbn){
		Map<String,Object> response = new HashMap<String,Object>();
		try {
			logger.info("iniciando la obtencion del libro con el isbn: "+isbn);
			response.put("Libro", libroService.buscarPorIsbn(isbn));
			response.put("Mensaje", "se encontro el objeto correctamente");
		}catch(ManagerException e){
			response.put("Mensaje", "Error al intentar consultar El objeto");
			response.put("Error", e.getMessage());
			return new ResponseEntity<Map<String,Object>>(response,HttpStatus.INTERNAL_SERVER_ERROR);
		}
		logger.info("se retorna la respuesta sin problemas de la obtencion"
				+ "del libro con el isbn: "+isbn);
		return new ResponseEntity<Map<String,Object>>(response,HttpStatus.OK);
	}
}
