package ar.edu.unju.fi.tpFinal.dto;

public class AlumnoDto extends MiembroDto {
	private static final long serialVersionUID = 1L;
	private String libretaUniversitaria;

	public String getLibretaUniversitaria() {
		return libretaUniversitaria;
	}

	public void setLibretaUniversitaria(String libretaUniversitaria) {
		this.libretaUniversitaria = libretaUniversitaria;
	}
}
